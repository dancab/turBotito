'''
Created on Feb 6, 2018

@author: nico
'''
import unittest


class Test(unittest.TestCase):


    def testEmailIO(self):
        from plugins.emailIO import writeEmail

        print 'write an email test'
        toList  = ['biocca.nicolas@gmail.com']
        subject = 'test message - unitTest'
        message = 'this a test message from unitTest' 
        writeEmail(toList,subject,message)

    def testTwitterIO(self):
        from plugins.twitterIO import writeTwitter
        
        print 'write a status test on Twitter'
        status = 'testing Twitter status function from unitTest'
#         writeTwitter(status)

    def testTelegramIO(self):
        from plugins.telegramIO import writeTelegram
        print 'write a message to someone'
        
        chatID  = 401369448              # nbiocca chatId (could be replaced by @nbiocca)
        message = 'testing Telegram function from unitTest'
        
        writeTelegram(chatID, message)
        
        # to send files
        # fileHandler = open(filename)
        # bot.send_document(chatID,fileHandler)
                

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()