'''
Created on Feb 7, 2018

@author: nico
'''
import unittest

import os
# to load keys in json format
import json
# to use config package directory (credentials are stored here)
import plugins

# for pretty prints
import pprint
from core.binance.binancecomm import binanceComm

fileName = 'notifierKeys.json'
fileName = os.path.join(os.path.dirname(plugins.__file__),fileName)
fileHandler = open(fileName,'r')                                   
credentials = json.load(fileHandler)                               

API_KEY = credentials['binance']['api_key']
API_SECRET = credentials['binance']['api_secret']

class TestBinance(unittest.TestCase):
    
    def test1(self):
        '''
        unit test for testing API wrapper capabilities
        '''
        from core.binance.client import Client
        client = Client(API_KEY, API_SECRET)
        
        print client
        pprint.pprint(dir(client))
        
        print ('get_products method:')
        client.get_products()

        print ('Account information:')
        account = client.get_account(recvWindow=2000)
        pprint.pprint(account, width=1)

        print ('get_symbol_info:')
        mkt = 'BTC'
        tgt = 'ETH'
        symbolInfo = client.get_symbol_info(tgt+mkt)
        pprint.pprint(symbolInfo,width=1)
        
        print ('get_order_book:')
        orderBook = client.get_order_book(symbol=tgt+mkt,limit=10)
        pprint.pprint(orderBook,width=1)
        
        print ('get_recent_trades:')
        trades = client.get_recent_trades(symbol=tgt+mkt,limit=10)
        pprint.pprint(trades,width=1)
        
        print('get_historical_klines (candles)')
        start  = 1519308000  # 02/22/2018 @ 2:00pm (UTC)
        end    = 1519315200  # 02/22/2018 @ 4:00pm (UTC)
        period = '30m'       # thirty minutes 
        candles = client.get_historical_klines(symbol   = tgt+mkt,
                                               interval = period,
                                               start_ts = start*1000,
                                               end_ts   = end*1000)
        print(candles.__class__)
        pprint.pprint(candles)
        
        print('get_aggregate_trades')
        start  = 1519308000  # 02/22/2018 @ 2:00pm (UTC)
        end    = 1519311600  # 02/22/2018 @ 3:00pm (UTC)
                             # more than 1 hour between start and end
                             # raise a BinanceAPIException
        trades = client.get_aggregate_trades(symbol    = tgt+mkt,
                                             startTime = start*1000,
                                             endTime   = end*1000)
        
        pprint.pprint(trades)
        print(trades.__class__)
        print(len(trades))
        
        print('get_recent_trades:')
        trades = client.get_recent_trades(symbol='BNBBTC')
        pprint.pprint(trades)
        
        
        
    def test2(self):
        '''
        binance communicator tests
        '''
        from core.binance import binancecomm # credentials are loaded on communicator        
        
        
        print('BINANCE COMUNICATOR: getChart')
        binance  = binanceComm()
        coinpair = 'BTC_ETH'
        start    = 1519308000   # 02/22/2018 @ 2:00pm (UTC) 
        end      = 1519315200   # 02/22/2018 @ 4:00pm (UTC) 
        period   = '30m'        # thirty minutes             

        chart = binance.getChart(coinpair = coinpair,
                                 period   = period,
                                 initime  = start,
                                 endtime  = end)
        
        print(chart.__class__)
        pprint.pprint(chart)
        
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()