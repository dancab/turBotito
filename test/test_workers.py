'''
Created on Feb 28, 2018

@author: nico
'''
import unittest
from duplicity.gpginterface import Process


class TestWorkers(unittest.TestCase):


    def testPoolOfWorkers(self):
        from multiprocessing import Process

        import plugins.emailIO
        import plugins.telegramIO
        
        import time
        
        # configuring process 1 (send a message by email)
        p1 = Process(target = plugins.emailIO.writeEmail)
        # setting args to be passsed to target
        toList  = ['biocca.nicolas@gmail.com']
        subject = 'test message - unitTest'          
        message = 'this is a test from ' + p1.name 
        args1 = (toList,subject,message)
        p1._args = args1
        
        
        # configuring process 2 (send a message by telegram)
        p2 = Process(target = plugins.telegramIO.writeTelegram)
        # setting args to be passed to target
        chatID  = 401369448         # nbiocca chatId (could be replaced by @nbiocca)
        message = 'this a test from ' + p2.name
        args2 = (chatID,message)
        p2._args = args2
        
        
        # start process 
        p1.start()
        p2.start()
        
        # sleep time
        print 'process 1 PID: ' + str(p1.pid)
        print 'process 2 PID: ' + str(p2.pid)
        time.sleep(30)
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'TestWorkers.testPoolOfWorkers']
    unittest.main()