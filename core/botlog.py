import os
import pandas as pd
import auxf

import pickle

# botito package
import output


class BotLog(object):

    def __init__(self, pair, period, nBacklogging):
        # each key provides a list of historical data
        self.data = pd.DataFrame()
        #  trades is a list containing all the trades open and closed
        self.closedTrades = []
        # openTrades is a list containing all trades still open
        self.openTrades = []
        # candlesticks is a list containing all candlesticks
        self.candlesticks = []
        # indicator keys
        self.indicatorKeys = []
        # dataframe keys
        self.keys = []
        # currency pair
        self.pair = pair
        # candle period
        self.period = period
        # priceLog for easy access
        self.priceLog = []
        # maximum and minimum of any kind of data storage
        self.extrema = {}
        self.extremaValues = dict()
        self.extremaDates = dict()
        # number of backlogging candles
        self.nBacklogging = nBacklogging

    def initData(self, indicatorKeys):
        # keys is a list of strings
        self.keys = ['date',
                'price',
                'mktVolume',
                'tgtVolume',
                'high',
                'low',
                'close',
                'open']
        # adds indicator keys
        self.indicatorKeys = indicatorKeys
        # append indicator keys to dataframe keys
        self.keys += indicatorKeys
        for key in self.keys:
            self.data[key] = []
        self.initExtrema()

    def initExtrema(self, keys=None):
            keys = ['price',
                    'mktVolume',
                    'tgtVolume']
            # adds indicator labels
            keys += self.indicatorKeys

            for key in keys:
                self.extrema[key] = {}
                self.extremaValues[key] = dict()
                self.extremaDates[key] = dict()

    def save(self, filename=None):
        # saves botlog instance as an object.
        # Comment:
        #   To restore the object it isn't necessary create an instance
        #   and then restore it. This makes it impossible to create a method 
        #   that restore it (impossible?). 
        #   To load the object: 
        #
        #   filehandler = open(filename,'r')
        #   dataLog = pickle.load(filehandler)
        #   filehandler.close()
        #   
        if filename is None:
            filename = 'dataLog.obj'
            filename = os.path.join(os.path.dirname(output.__file__), filename)
        
        filehandler = open(filename, 'w')
        pickle.dump(self, filehandler)

        filehandler.close()

    def addNewRow(self):
        self.data.loc[len(self.data)] = [None for i in range(len(self.data.columns))]
        # check this simple method, put None over entire raw
        # self.data.loc[len(self.data)] = None

    def recData(self, key, newVal):
        # if key is not yet present in data, create the list
        if not key in self.data.columns:
            raise ValueError('ERROR, unexistent key in log.data')
        lastRow = len(self.data) - 1
        self.data.loc[lastRow, key] = newVal

    def recCandlestick(self, candle):

        if candle.status == 'empty':
            self.recData('price', self.getLastVal('price'))
            self.recData('open', self.getLastVal('price'))
            self.recData('close', self.getLastVal('price'))
        else:
            self.recData('price',candle.price)
            self.recData('open',candle.open)
            self.recData('close',candle.close)
        self.recData('date',candle.date)
        self.recData('mktVolume',candle.mktVolume)
        self.recData('tgtVolume',candle.tgtVolume)
        self.recData('high',candle.high)
        self.recData('low',candle.low)


    def getCol(self, key, ignoreLast=False):
        if ignoreLast:
            # take all values of a column except the last one
            # because the last value is not written yet
            return self.data.loc[:, key].values[:-1]
        else:
            # includes last value
            # MOST USED CASE 
            return self.data.loc[:, key].values

    def updatePriceLog(self):
        self.priceLog = self.getCol('price')

    def getVal(self, index, key):
        return self.getCol(key)[index]

    def getLastVal(self, key):
        # get last value ignoring present row
        return self.getCol(key)[-2]

    def getPVal(self, key):
        # get present value as float
        return self.getCol(key)[-1]

    def getRow(self, index):
        return dict(zip(self.data.columns, self.data.iloc[index, :].values))

    def getPRow(self):
        # get present row as dictionary
        return dict(zip(self.data.columns, self.data.iloc[-1, :].values))    

    def showPRow(self, keys=[]):
        # show present row as string
        PRow = self.getPRow()
        Msg = 'date: ' + str(auxf.createTimeString(PRow['date']))
        del PRow['date']
        if len(keys) > 0:
            for key in keys:
                Msg = Msg + '\t' + key + ': ' + str(PRow[key])            
        else:
            for key in PRow:
                Msg = Msg + '\t' + key + ': ' + str(PRow[key])
        print Msg

    def prePrintPRow(self, keys=[]):
        # show present row as string
        PRow = self.getPRow()
        Msg = 'date: ' + str(auxf.createTimeString(PRow['date']))
        del PRow['date']
        if len(keys) > 0:
            for key in keys:
                Msg = Msg + '\t' + key + ': ' + str(PRow[key])            
        else:
            for key in PRow:
                Msg = Msg + '\t' + key + ': ' + str(PRow[key])
        return Msg

    def addTrade(self, newTrade):
        self.openTrades.append(newTrade)

    def rmTrade(self, oldTradeIP, oldTrade):
        self.closedTrades.append(oldTrade)
        del self.openTrades[oldTradeIP]

