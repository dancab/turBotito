from poloniex_full import Poloniex, PoloniexError
from core.orderbook import OrderBook
from core.balance import Balance
from core.trade import Trade

class PoloniexComm(object):
    def __init__(self, account=None):
        self.account = account
        self.polo = Poloniex(key=self.account.key, secret=self.account.secret,
            timeout=80, coach=True, jsonNums=False)

    def assignAccount(self, account):
        self.account = account

    def getChart(self, coinPair, period, initime, endtime):
        return self.polo.returnChartData(coinPair, period, initime, endtime)

    def getTrades(self, coinPair, initime=False, endtime=False):
        return self.polo.marketTradeHist(coinPair, initime, endtime)

    def getBalances(self):
        # get poloniex balances
        poloBalance = self.polo.returnBalances()
        # convert to tito format
        balance = Balance.fromPoloniexBalance(poloBalance)
        # return
        return balance

    def getOrderBook(self, coinPair):
        # get poloniex order book
        poloOrderBook = self.polo.returnOrderBook(coinPair)
        # convert to tito format
        orderBook = OrderBook.fromPoloniexOrderBook(coinPair, poloOrderBook)
        # return
        return orderBook

    def buy(self, coinPair, rate, amount, orderType=False):
        # buy
        orderData = self.polo.buy(coinPair, rate, amount, orderType)
        orderNumber = orderData['orderNumber']
        resultingTrades = orderData['resultingTrades']
        # convert trades to tito format
        trades = list()
        for poloTrade in resultingTrades:
            trade = Trade.fromPoloniexTrade(coinPair, poloTrade)
            trades.append(trade)
        # return
        return orderNumber, trades


    def sell(self, coinPair, rate, amount, orderType=False):
        # sell
        orderData = self.polo.sell(coinPair, rate, amount, orderType)
        orderNumber = orderData['orderNumber']
        resultingTrades = orderData['resultingTrades']
        # convert trades to tito format
        trades = list()
        for poloTrade in resultingTrades:
            trade = Trade.fromPoloniexTrade(coinPair, poloTrade)
            trades.append(trade)
        # return
        return orderNumber, trades

    def getOpenOrders(self, coinPair='all'):
        return self.polo.returnOpenOrders(currencyPair=coinPair)

    def getOrderTrades(self, orderID):
        try:
            return self.polo.returnOrderTrades(orderID)
        except PoloniexError:
            return None

    def cancelOrder(self, orderID):
        return self.polo.cancelOrder(orderID)