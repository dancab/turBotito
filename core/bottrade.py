import auxf


class BotTrade(object):

    def __init__(self, coinBox, mktCurrency, tgtCurrency):
        # mkt = mortal kombat... ok, no.
        # mkt = market, indicated the market currency
        # tgt = target, indicates the target currency
        # example: BTC_ETH, BTC is the market, ETH is the target
        self.status = ''
        self.entryPrice = ''
        self.entryMktAmount = ''
        self.entryTgtAmount = ''
        self.entryDate = ''
        self.exitPrice = ''
        self.exitMktAmount = ''
        self.exitTgtAmount = ''
        self.exitDate = ''
        self.profit = ''
        self.cometa = ''
        # markett coin is the existent coin before transactions
        self.mktCurrency = mktCurrency
        # target coin is the coin to buy with the market coin
        self.tgtCurrency = tgtCurrency
        # coinBox is the balance of all coins
        self.coinBox = coinBox
    
    def trade(self):
        # function to trade one coin for another
        pass

    def openTrade(self, price, fee, amount, date):
        # buy base currency against quote currency
        self.status = 'OPEN'
        # price of the base measured in quote
        self.entryPrice = price
        # amount of market currency that is expended
        self.entryMktAmount = amount
        # poloniex's cut (in market currency)
        cometa = amount * fee
        self.entryMktCometa = cometa
        self.entryTgtCometa = cometa / price
        # amount of target currency that is acquired
        self.entryTgtAmount = (amount - cometa) / price
        # currency box flow
        self.coinBox.outflux(self.mktCurrency, self.entryMktAmount)
        self.coinBox.influx(self.tgtCurrency, self.entryTgtAmount)
        # entry date
        self.entryDate = date

    def closeTrade(self, price, fee, date):
        self.status = 'CLOSED'
        self.exitPrice = price
        # amount of target that is sold (all of it as of now)
        amount = self.entryTgtAmount
        self.exitTgtAmount = amount
        # poloniex's cut (in target currency)
        cometa = amount * fee
        self.exitTgtCometa = cometa
        self.exitMktCometa = cometa * price
        # amount of quote that is acquired back
        self.exitMktAmount = (amount - cometa) * price
        self.profit = self.exitMktAmount - self.entryMktAmount
        self.cometa = self.entryMktCometa + self.exitMktCometa
        self.coinBox.outflux(self.tgtCurrency, self.exitTgtAmount)
        self.coinBox.influx(self.mktCurrency, self.exitMktAmount)
        # exit date
        self.exitDate = date

    def prePrint(self):
        Msg = 'Status: ' + str(self.status) + '\t' + \
              'Entry Price: ' + str(self.entryPrice) + '\t'
        if self.status is 'CLOSED':
            Msg = Msg + 'Exit Price: ' + str(self.exitPrice) + '\t' + \
                        'entryAmount: ' + str(self.entryMktAmount) + '\t' + \
                        'exitAmount: ' + str(self.exitMktAmount) + '\t' + \
                        'Profit: ' + str(self.profit) + '\t' + \
                        'Cometa:' + str(self.cometa)
        return Msg

    def showTrade(self):
        Msg = 'Status: ' + str(self.status) + '\t' + \
              'Entry Price: ' + str(self.entryPrice) + '\t'
        if self.status is 'CLOSED':
            Msg = Msg + 'Exit Price: ' + str(self.exitPrice) + '\t' + \
                        'entryAmount: ' + str(self.entryMktAmount) + '\t' + \
                        'exitAmount: ' + str(self.exitMktAmount) + '\t' + \
                        'Profit: ' + str(self.profit) + '\t' + \
                        'Cometa:' + str(self.cometa)

        print Msg
