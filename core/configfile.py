from auxf import confirmContinue

SECTION_KEYS = ['Exchange', 'Market', 'Operation']
PARAMETER_KEYS = dict(
    Exchange = ['Exchange Name', 'Key', 'Secret'],
    Market = ['Market Coin', 'Target Coin'],
    Operation = ['Type', 'Method', 'Rate', 'Limit Rate', 'Amount'] )

class ConfigFile(object):
    def __init__(self, fileStr):
        self.fileStr = fileStr
        self.status = 'blank'
        self.__fileLines = list()
        self.__sectionKeys = []
        self.__sectionKeysIndices = []
        # initialize a blank dictionary with None for values
        self.data = dict()
        for sectionKey in SECTION_KEYS:
            self.data[sectionKey] = dict()
            for paramKey in PARAMETER_KEYS[sectionKey]:
                self.data[sectionKey][paramKey] = None

    def __readLines(self):
        # read file and store it in a list of lines
        # open file
        try:
            file = open(self.fileStr, 'r')
            self.status = 'read'
        except IOError:
            print 'No configuration file found with name: ' + self.fileStr
            print 'Template file can be created'
            self.writeFile()
            return 0
        # read all lines and split into list and strip of trailing spaces
        self.__fileLines = file.read().splitlines()
        file.close()

    def __getSectionKeys(self):
        # cycle lines to extract section keys present
        tup = [(i, line) for i, line in enumerate(self.__fileLines) if line and line[0] != ' ' and line.strip()[-1] == ':']
        self.__sectionKeysIndices = [item[0] for item in tup]
        self.__sectionKeys = [item[1].strip().replace(':','') for item in tup]

    def __readSections(self):
        # cycle section keys to read them from the file lines
        for sectionKeyIndex, sectionKey in zip(self.__sectionKeysIndices, self.__sectionKeys):
            if sectionKey not in SECTION_KEYS:
                raise KeyError('Wrong section key: ' + sectionKey + ' --- --- Check configuration file: ' + self.fileStr) 
            sectionData = self.__readSection(sectionKeyIndex, sectionKey)
            for paramKey in sectionData:
                self.data[sectionKey][paramKey] = sectionData[paramKey]

    def __readSection(self, sectionKeyIndex, sectionKey):
        # cycle lines composing a section to extract that section parameters
        sectionData = dict()
        i = sectionKeyIndex
        while True:
            i += 1
            # check if section ended
            line = self.__fileLines[i]
            if 'End' in line:
                break
            # -----
            lineData = line.split('=')
            lineData = [s.strip() for s in lineData]
            paramKey, paramVal = lineData
            if paramKey not in PARAMETER_KEYS[sectionKey]:
                raise KeyError('Wrong key in ' + sectionKey + ' section' + ' --- --- Check configuration file' + self.fileStr)
            sectionData[paramKey] = paramVal
        return sectionData

    def readFile(self):
        self.__readLines()
        self.__getSectionKeys()
        self.__readSections()

    def prePrint(self):
        lines = []
        for sectionKey in SECTION_KEYS:
            lines.append(sectionKey+':')
            for paramKey in PARAMETER_KEYS[sectionKey]:
                lines.append( '    ' + paramKey + ' = ' + str(self.data[sectionKey][paramKey]) ) 
            else:
                lines.append( 'End ' + sectionKey)
                lines.append( '' )
        return lines

    def printData(self, lines=False):
        if lines == False:
            lines = self.prePrint()
        for line in lines:
            print line

    def writeFile(self):
        lines = self.prePrint()
        print 'Data to write to configuration file: '
        self.printData(lines) 
        segui = confirmContinue()
        if segui != True:
            return 0
        else:
            self.status = 'written'
            file = open(self.fileStr, 'w')
            for line in lines:
                file.write(line+'\n')
            file.close()

    def setParameter(self, sectionKey, paramKey, paramVal):
        if sectionKey not in SECTION_KEYS:
            return 1
        if paramKey not in PARAMETER_KEYS[sectionKey]:
            return 1
        if paramVal == '':
            return 1
        self.data[sectionKey][paramKey] = paramVal