'''
Created on Feb 11, 2018

@author: nico
'''

from client import Client

class binanceComm(object):
    def __init__(self):
        import os
        # to load keys in json format
        import json
        # to use config package directory (credentials are stored here)
        import plugins
        
        fileName = 'notifierKeys.json'
        fileName = os.path.join(os.path.dirname(plugins.__file__),fileName)
        fileHandler = open(fileName,'r')                                   
        credentials = json.load(fileHandler)                               
        
        API_KEY    = credentials['binance']['api_key']
        API_SECRET = credentials['binance']['api_secret']
        
        self.client = Client(api_key    = API_KEY, 
                             api_secret = API_SECRET)
    
    def assignAccount(self, account):
        '''
        @deprecated: binance wrapper hace un ping al inicio de clase Client
        '''
        pass
        
    def getChart(self, coinpair, period, initime, endtime):
        '''
        :param coinpair: Name of symbol pair e.g BTC_ETH. mkt_tgt format  
        :type coinpair: str
        :param period: candlestick period
        :type period: not standardized yet
        :param initime: start time in unix timestamp format (seconds)
        :type initime: int
        :param endtime: end time unix timestamp format (seconds)
        :type endtime: int
        @todo: pensar que fomato standard deberia tener period
               string, integer ??. Por el momento period es str
        @todo: compute weightedAverage
        @warning: weightedAverage not computed!! 
        '''
        mkt = coinpair.split('_')[0]
        tgt = coinpair.split('_')[1]
        
        
        candleList = self.client.get_historical_klines(symbol = tgt+mkt,
                                                       interval = period,
                                                       start_ts = initime*1000,# to milliseconds
                                                       end_ts = endtime*1000)  # to milliseconds
        
        # response format
#         [
#           [
#             1499040000000,      // Open time
#             "0.01634790",       // Open
#             "0.80000000",       // High
#             "0.01575800",       // Low
#             "0.01577100",       // Close
#             "148976.11427815",  // Volume
#             1499644799999,      // Close time
#             "2434.19055334",    // Quote asset volume
#             308,                // Number of trades
#             "1756.87402397",    // Taker buy base asset volume
#             "28.46694368",      // Taker buy quote asset volume
#             "17928899.62484339" // Ignore
#           ]
#         ]
        # AHORA DEBERIA TRANSFOMAR LA SALIDA A FORMATO STANDARD
        output = [] # define list 
        
        for candle in candleList:
            candlestick = {'volume'         : candle[5],
                           'quoteVolume'    : candle[7],
                           'high'           : candle[2],
                           'low'            : candle[3],
                           'date'           : candle[0]/1000,
                           'close'          : candle[4],
                           'weightedAverage': 0,
                           'open'           : candle[2]}
            output.append(candlestick)
        
        return output
        
    def getTrades(self, coinpair, initime=False, endtime=False):
        '''
        @return: trades for a given coinpair between initime and endtime
        :param coinpair: Name of symbol pair e.g BTC_ETH. mkt_tgt format
        :type coinpair: str
        :param initime: start time in unix timestamp format (seconds)
        :type initime int
        :param endtime: end time unix timestamp format (seconds)
        :type endtime: int
        @warning: not finished
        '''
        mkt = coinpair.split('_')[0]
        tgt = coinpair.split('_')[1]
        
        tradeList = self.client.get_aggregate_trades(symbol = tgt+mkt,
                                                  startTime = initime*1000,
                                                  endTime   = endtime*1000)


        # response format
#         [
#           {
#             "a": 26129,         // Aggregate tradeId
#             "p": "0.01633102",  // Price
#             "q": "4.70443515",  // Quantity
#             "f": 27781,         // First tradeId
#             "l": 27781,         // Last tradeId
#             "T": 1498793709153, // Timestamp
#             "m": true,          // Was the buyer the maker?
#             "M": true           // Was the trade the best price match?
#           }
#         ]
        output = []
        for trade in tradeList:
#             tradeDict = {'tradeID':,
#                          'amount':,
#                          'total':,
#                          'rate':,
#                          'date':,
#                          'type':,
#                          ''}
            pass
        









        