class CoinBox(object):

    def __init__(self, initialBalance={}):
        # balance     should be a dictionary
        # self.balance    ['BTC'] = balance    ['BTC']
        self.balance = initialBalance    

    def influx(self, coin, amount):
        if coin not in self.balance:
            self.balance[coin] = 0
        self.balance[coin] += amount

    def outflux(self, coin, amount):
        if coin not in self.balance:
            exit('ERROR: cannot take unexistent coin out of the box')
        self.balance[coin] -= amount
