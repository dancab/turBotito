import auxf


class CandleStick(object):

    def __init__(self):
        # market volume = volume in first currency of pair
        self.mktVolume = 0
        # target volume = volume in second currency of pair
        self.tgtVolume = 0
        # high = highest price of period
        self.high = 0
        # low = lowest price of perios
        self.low = 0
        # date = date of period (opening date)
        self.date = 0
        self.closingDate = 0
        # close = closing price of period
        self.close = 0
        # weightedAverage = average price of period weighted with transaction volume
        self.price = 0
        # open = opening price of period
        self.open = 0
        # list of trades that form the candle (optional)
        self.trades = []
        # candlePeriod
        self.period = 0
        # status
        self.status = 'blank'

    def assignTimes(self, date, period):
        self.status = 'assembling'
        self.date = date
        self.period = period
        self.closingDate = date + period

    def addTrade(self, trade):
        if not self.status == 'assembling':
            raise ValueError('cannot add trades, candle is not in assembling status')
        self.trades.append(trade)

    def assemble(self):
        if not self.status == 'assembling':
            raise ValueError('cannot assemble, candle is not in assembling status')
        if not self.trades:
            self.status = 'empty'
            return 1
        else:
            self.status = 'assembled'
        # ---------------------------------
        # set values to zero before adding
        self.price = 0
        self.mktVolume = 0
        self.tgtVolume = 0
        # ---------------------------------
        # start adding
        for trade in self.trades:
            # assign first price to high and low to compare later
            self.high = trade.price
            self.low = trade.price
            # add volumes
            self.mktVolume += trade.mktAmount
            self.tgtVolume += trade.tgtAmount
            # add weighted prices
            self.price += trade.price * trade.tgtAmount
            # check if new high
            if trade.price > self.high:
                self.high = self.price 
            # check if new low
            if trade.price < self.low:
                self.low = self.price
        # the price is a weighted average
        self.price = self.price / self.tgtVolume
        # ---------------------------------
        # set open and close prices
        self.open = self.trades[0].price
        self.close = self.trades[-1].price
        # ---------------------------------

    def assignPrice(self, price):
        self.price = price

    def assignData(self, candleDict):
        # assigns data from a dictionary candle into this candle object
        # -------------------------------------------------------------
        if not self.status == 'blank':
            raise ValueError('cannot assign, candle is not in blank status')
        self.status = 'assigned'
        # mkt = market 
        # tgt = target
        self.mktVolume = float(candleDict['volume'])
        self.tgtVolume = float(candleDict['quoteVolume'])
        self.high = float(candleDict['high'])
        self.low = float(candleDict['low'])
        self.date = int(candleDict['date'])
        self.close = float(candleDict['close'])
        self.price = float(candleDict['weightedAverage'])
        self.open = float(candleDict['open'])

    def resetCandle(self):
        self.__init__()

    def prePrint(self):
        # prepares a message with information to be printed as the candle
        msg = 'date: ' + str(auxf.uts2str(self.date)) + ' -- '  \
        + 'price: ' + str(self.price) + ' -- ' \
        + 'tgtVolume: ' + str(self.tgtVolume)
        return msg

    def showCandle(self, extraKeys=[], extraData=''):
        Msg = 'Time: ' + str(self.date) + '\t' + \
              'Price: ' + str(self.price)

        for key in extraKeys:
            Msg = Msg + '\t' + key + ': ' + str(extraData.getVal(-1, key))

        print Msg
