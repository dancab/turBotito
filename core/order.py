class Order(object):
    def __init__(self, tipo, coinPair, amount):
        self.number = 0
        self.orderType = orderType
        self.trades = list()
        self.coinPair = coinPair
        self.intendedAmount = amount
        self.filledAmount = 0.0
        self.unfilledAmount = amount

    def computeAmounts(self):
        self.filledAmount = 0.0
        for trade in self.trades:
            self.filledAmount += trade.tgtAmount
        self.unfilledAmount = self.intendedAmount - self.filledAmount

    def show(self):
        print '====================================='
        print 'Order Number: ' + str(self.number)
        print 'Type: ' + str(self.orderType)
        print 'Coin Pair: ' + str(self.coinPair)
        print 'Intended amount: ' + str(self.intendedAmount)
        print 'Filled amount: ' + str(self.filledAmount)
        print 'Unfilled amount: ' + str(self.unfilledAmount)
        print 'Trades: '
        for trade in self.trades:
            trade.show()
        print '====================================='