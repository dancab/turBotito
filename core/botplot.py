#!/usr/bin/python
# Filename: botplotV2.py

from math import pi
from time import time
import pandas as pd
from bokeh.plotting import figure, output_file, show, save, ColumnDataSource

# from bokeh.io import gridplot
from bokeh.layouts import gridplot

from bokeh.models import LinearAxis, Range1d, HoverTool
from bokeh.models import NumeralTickFormatter
# from bokeh.models import PrintfTickFormatter  # to set different axis formats

from bokeh.layouts import layout
from bokeh.models.widgets import Panel, Tabs

import numpy as np

from scipy.signal import argrelextrema

import sys
import pickle
import datalog


class BotPlot(object):

	def __init__(self, log):

		# self.df = pd.DataFrame()
		# pointer to log dataframe
		# self.df  = log.data
		# pointer to log object 
		self.log = log
		# initialize pointer
		self.logTrades = self.log[0]

		# initialize ith dataLog
		self.dataLogi = self.log[0]  # self.logTrade will be deprecated

		# dataFrame with trades data
		self.dfTrade = pd.DataFrame()

		# Windows Settings 
		self.plotWidth = 1000
		self.plotHeight = 450

		# set output fileName
		self.fileName = 'OUTPUT.html'

		# tool bar options
		self.tools = "pan,wheel_zoom,box_zoom,crosshair,reset,save"

		# erase everything related to backlogging
		# drop backlogging data
		for i in range(len(log)):
			try:
				n = log[i].nBacklogging
			except:
				n = 100
			# restore pandas dataFrame
			log[i].data = log[i].data.drop(log[i].data.index[0:n])  # drop data from backlogging --> Now index starts from n
			newIndex = log[i].data.index - log[i].data.index[0]  # creates newIndex starting from 0
			log[i].data.index = newIndex  # now dataFrame backtest data starts from index 0

			# restore extrema and extremaValues
			extrema 	 = log[i].extrema
			extremaValues = log[i].extremaValues
			keysExtrema = log[i].extrema.keys()
			for key in keysExtrema:
				# take into account two possible initilizations of extrema
				try:
					# future initilization (max/min as None at least)
					if extrema[key]['max'] or extrema[key]['min'] is None:
						continue
				except:
					# present initilization
					if len(extrema[key]) is 0:
						extrema[key]['max'] 	 = None  # initialize format
						extrema[key]['min'] 	 = None  # initialize format
						extremaValues[key]['max'] = None  # initialize format
						extremaValues[key]['min'] = None  # initialize format
						continue 
				
				dummy1 = extrema[key]['max']  # pointer to numpy array
				dummy2 = extremaValues[key]['max']
				index = dummy1 > n
				dummy1 = dummy1[index] - n  # according to new Indew   
				dummy2 = dummy2[index]
				
				extrema[key]['max'] 	 = dummy1
				extremaValues[key]['max'] = dummy2

				# print dummy1
				# print extrema[key]['max']

				dummy1 = extrema[key]['min']  # pointer to numpy array
				dummy2 = extremaValues[key]['min']
				index = dummy1 > n
				dummy1 = dummy1[index] - n  # according to new Indew
				dummy2 = dummy2[index]

				extrema[key]['min'] 	 = dummy1
				extremaValues[key]['min'] = dummy2

	def plotCandlestick(self, p):

		# Plot candle 'shadows'/wicks
		p.segment(x0=self.df.date,
				  y0=self.df.high,
				  x1=self.df.date,
				  y1=self.df.low,
				  color="black",
				  line_width=2)

		# Plot green candles
		inc = self.df.close > self.df.open
		p.vbar(x=self.df.date[inc],
			   width=self.candlestickWidth,
			   top=self.df.open[inc],
			   bottom=self.df.close[inc],
			   fill_color="green",
			   line_width=0.5,
			   line_color='black')

		# Plot red candles
		dec = self.df.open > self.df.close
		p.vbar(x=self.df.date[dec],
			   width=self.candlestickWidth,
			   top=self.df.open[dec],
			   bottom=self.df.close[dec],
			   fill_color="red",
			   line_width=0.5,
			   line_color='black')

	def plotSMA(self, p, SMAkey):
		p.line(x=self.df.loc[:, 'date'],
				y=self.df.loc[:, SMAkey],
				color='orange',
				alpha=0.9,
				line_width=3,
				legend=SMAkey,
				name="smaLine")

	def plotEMA(self, p, EMAkey):
		p.line(x=self.df.loc[:, 'date'],
				y=self.df.loc[:, EMAkey],
				color='red',
				alpha=1.0,
				line_width=3,
				legend=EMAkey,
				name="EMAline")		

	def plotVolume(self, p):

		axisStart = 0  # min(self.df['volume'])
		axisEnd = 1.5 * max(self.df.mktVolume)
		p.extra_y_ranges = {'volume': Range1d(start=axisStart, end=axisEnd)}
		p.add_layout(LinearAxis(y_range_name='volume'), 'right')

		upcolor = "green"
		downcolor = "red"

		# Plot green candles
		inc = self.df.close > self.df.open
		p.vbar(x=self.df.date[inc],
			   width=self.candlestickWidth,
			   top=self.df.mktVolume[inc],
			   bottom=0,
			   alpha=0.2,
			   fill_color=upcolor,
			   line_color=upcolor,
			   y_range_name='volume')

		# Plot red candles
		dec = self.df.open > self.df.close
		p.vbar(x=self.df.date[dec],
			   width=self.candlestickWidth,
			   top=self.df.mktVolume[dec],
			   bottom=0,
			   alpha=0.2,
			   fill_color=downcolor,
			   line_color=downcolor,
			   y_range_name='volume')    

	def plotRSI(self, p, n=14, upperBound=80, lowerBound=20,
				colorRSI='black', lineWidthRSI=1,
				colorBound='black', lineWidthBound=1):

		p.line(x=self.df.date,
				y=self.df['RSI'],
				color=colorRSI,
				line_width=lineWidthRSI,
				legend='RSI')

		# Upper bound line
		p.segment(x0=self.df.date.head(1),
				  y0=upperBound,
				  x1=self.df.date.tail(1),
				  y1=upperBound,
				  color=colorBound,
				  line_width=lineWidthBound,
				  legend='RSI')

		# Lower bound line
		p.segment(x0=self.df.date.head(1),
				  y0=lowerBound,
				  x1=self.df.date.tail(1),
				  y1=lowerBound,
				  color=colorBound,
				  line_width=lineWidthBound,
				  legend='RSI')        

	def plotMACD(self, p,
				 colorMACD='black', lineWidthMACD=1,
				 colorStrikeLine='orange', lineWidthStrikeLine=1):

		p.line(x=self.df.date,
				y=self.df.MACD,
				color=colorMACD,
				line_width=lineWidthMACD,
				legend='MACD')
				# y_range_name = 'MACD')

			# p.yaxis[0].formatter = NumeralTickFormatter(format='0.00000')
		# p.yaxis[1].formatter = PrintfTickFormatter(format='5.3f') # format = '4.1e' 

		# Zero line
		p.segment(x0=self.df.date.head(1),
				  y0=0,
				  x1=self.df.date.tail(1),
				  y1=0,
				  color='black',
				  line_width=1,
				  # y_range_name = 'MACD',
				  legend='MACD')

		p.legend.orientation = 'horizontal'    
		p.legend.label_text_font_size = '8pt'    

	def plotSL(self, p, colorSL='blue', lineWidthSL=1):

		p.line(x=self.df.date,
				y=self.df.SL,
				color=colorSL,
				line_width=lineWidthSL,
				legend='SL')
				# y_range_name = 'MACD')

		p.legend.orientation = 'horizontal'
		p.legend.label_text_font_size = '8pt'

	def plotMFI(self, p, colorMFI='green', lineWidthMFI=1):

		p.line(x=self.df.date,
				y=self.df.MFI,
				color=colorMFI,
				line_width=lineWidthMFI,
				legend='MFI')  # ,
				# y_range_name = 'MACD')
		p.legend.orientation = 'horizontal'
		p.legend.label_text_font_size = '8pt'

	def plotHist(self, p, colorHist='orange', lineWidthHist=1):
		
		MACD_SL = (self.df.MACD - self.df.SL).values

		# green candles
		inc = MACD_SL >= 0
		p.vbar(x=self.df.date[inc],
			   width=self.candlestickWidth,
			   top=MACD_SL[inc],
			   bottom=0,
			   fill_color="green",
			   line_width=0.5,
			   line_color='black',
			   legend='Hist')
			   # y_range_name = 'MACD')

		# red candles
		dec = MACD_SL < 0
		p.vbar(x=self.df.date[dec],
			   width=self.candlestickWidth,
			   top=0,
			   bottom=MACD_SL[dec],
			   fill_color="red",
			   line_width=0.5,
			   line_color='black',
			   legend='Hist')
			   # y_range_name = 'MACD')

		p.legend.orientation = 'horizontal'
		p.legend.label_text_font_size = '8pt'        

	def plotExtrema_generic(self, p,
							extremaName=None,
							colorExtrema='red'):

		if extremaName is None:
			raise NotImplementedError('forget input extramaName')

		# search extremaName in extrema keys
		if not bool(set(extremaName) & set(self.dataLogi.extrema.keys())) :
			raise NotImplementedError('extremaName not exist on computed extrema keys')

		# auxiliar sets
		set1 = set(extremaName)
		set2 = set(self.dataLogi.extrema.keys())
		# intersects extremaName with available extrema keys 
		extremaName = list(set1.intersection(set2))

		for keyName in extremaName:
			# cycle if there is no extrema data for keyName
			if self.extrema[keyName]['max'] is None:
				continue
			# plot maxima
			imaxima = self.extrema[keyName]['max']
			p.circle(x=self.df.date[imaxima],
					 y=self.extremaValues[keyName]['max'],
					 size=15,
					 line_color=colorExtrema,
					 fill_color=colorExtrema)

			# plot minima
			iminima = self.extrema[keyName]['min']
			p.circle(x=self.df.date[iminima],
					 y=self.extremaValues[keyName]['min'],
					 size=15,
					 line_color=colorExtrema,
					 fill_color=colorExtrema)

	def plotExtrema(self, p):

		# finding local minima/maxima
		prices = self.df.price.values
		# Becareful, scipy method return tuples, [0] --> numpy array
		maxima = argrelextrema(prices, np.greater, order=10)[0]
		minima = argrelextrema(prices, np.less, order=10)[0]

		nMax = len(maxima)
		nMin = len(minima)

		# plot maxima
		p.circle(x=self.df.date[maxima + 100],
				 y=prices[maxima],
				 size=15,
				 line_color='black',
				 fill_color='black')

		# plot minima
		p.circle(x=self.df.date[minima + 100],
				 y=prices[minima],
				 size=15,
				 line_color='red',
				 fill_color='red')

	def plotTrades(self, p):

		self.dfTrade = pd.DataFrame()
		# initialize dataFrame columns
		self.dfTrade['buyDate']   	 = []
		self.dfTrade['buyPrice']  	 = []
		self.dfTrade['sellDate']  	 = []
		self.dfTrade['sellPrice'] 	 = []
		self.dfTrade['profit'] = []
		self.dfTrade['fee'] = []

		# number of trades
		nTrades = len(self.logTrades.closedTrades)
		for i in range(nTrades):
			# create space for data
			self.dfTrade.loc[i] = None
			# put data
			self.dfTrade.loc[i, 'buyDate'] = self.logTrades.closedTrades[i].entryDate
			self.dfTrade.loc[i, 'buyPrice'] = self.logTrades.closedTrades[i].entryPrice
			self.dfTrade.loc[i, 'sellDate'] = self.logTrades.closedTrades[i].exitDate
			self.dfTrade.loc[i, 'sellPrice'] = self.logTrades.closedTrades[i].exitPrice
			self.dfTrade.loc[i, 'profit'] = self.logTrades.closedTrades[i].profit
			self.dfTrade.loc[i, 'fee'] = self.logTrades.closedTrades[i].cometa
		
		# converts unix timestamp date format to a friendly format with pandas
		self.dfTrade['buyDate'] = pd.to_datetime(self.dfTrade['buyDate'], unit='s')
		self.dfTrade['sellDate'] = pd.to_datetime(self.dfTrade['sellDate'], unit='s')

		# creates trade performance (%)
		self.dfTrade['performance'] = (self.dfTrade['sellPrice'] / self.dfTrade['buyPrice'] - 1.0) * 100

		# create ColumnDataSource: maps names of columns to sequences or arrays
		# needed to customize hover tools
		source = ColumnDataSource(self.dfTrade)

		c1 = p.circle(x='buyDate',  # self.dfTrade.buyDate,
				 y='buyPrice',  # self.dfTrade.buyPrice,
				 size=15,
				 line_color='navy',
				 fill_color='orange',
				 fill_alpha=1,
				 legend='buy Orders',
				 name='buyOrders',
				 source=source)

		c2 = p.circle(x='sellDate',  # self.dfTrade.sellDate,
				 y='sellPrice',  # self.dfTrade.sellPrice,
				 size=15,
				 line_color='black',
				 fill_color='gray',
				 fill_alpha=1,
				 legend='sell Orders',
				 name='sellOrders',
				 source=source)

		hover1 = HoverTool(tooltips=[
			('trade', '$index'),
			('buy price', '@buyPrice'),
			],
			renderers=[c1])

		hover2 = HoverTool(tooltips=[
			('trade', '$index'),
			('buy price', '@buyPrice'),
			('sell price', '@sellPrice'),
			('profit', '@profit'),
			('performance', '@performance %')
			],
			renderers=[c2])

		p.add_tools(hover1)
		p.add_tools(hover2)

	def graph(self,
			  plotList=[],
			  keys1=[],
			  keys2=[],
			  keys3=[]):

		# number of market-target figures
		nTabs = len(plotList)
		nLayouts = nTabs

		# initialize list
		layouts = []
		tabs = []

		output_file(self.fileName)

		for i in range(len(self.log)):
			if self.log[i].pair not in plotList:
				continue
			# setup specific market-target parameters
			self.df = self.log[i].data
			# drop backlogging data
				# self.df = self.df.drop(self.df.index[0:100])
			# // will be deprecated in a near future by reindex approach //
			# // newIndex = self.df.index - self.df.index[0]				//
			# // self.df.index = newIndex								//
			# put data frame columns in human readable format
			self.df['date'] = pd.to_datetime(self.df['date'], unit='s')

			# some pointers
			self.dataLogi 	 = self.log[i]
			self.extrema  	 = self.log[i].extrema
			self.extremaValues = self.log[i].extremaValues

			# points to component i of log list of dataLog's objects
			self.logTrades = self.log[i]

			# set candlestick width
			self.candlestickWidth = self.log[i].period * self.plotWidth

			# set pair name
			self.pairName = self.log[i].pair

			# set title for figure 1
			self.title = self.log[i].pair + "-Poloniex"
	
			# Set of implementation errors
			if len(keys1) == 0:
				print 'no keys in figure 1 to assemble plot'
				return 0
	
			if len(keys2) == 0:
				print 'no keys to figure 2'
				raise NotImplementedError('error when no keys to figure 2')

			if len(keys3) == 0:
				print 'no keys to figure 3'
				raise NotImplementedError('error when no keys to figure 3')
			
			# ===============================================================================
			# CREATE FIGURES
			# 
			# figure 1 
			figure1 = figure(
				x_axis_type="datetime",
				y_range=(1.0 * min(self.df['low'].values), 1.0 * max(self.df['high'].values)),
				tools=self.tools,
				# tools=[hover,],
				title=self.title,
				plot_width=self.plotWidth,
				plot_height=self.plotHeight,
				toolbar_location="above")
			figure1.xaxis.major_label_orientation = pi / 4
			figure1.grid.grid_line_alpha = 0.7

			# figure 2
			axisStart = 0.8 * min(self.df.MACD)
			axisEnd = 1.5 * max(self.df.MACD)
			figure2 = figure(
				x_axis_type='datetime',
				x_range=figure1.x_range,
				y_range=(axisStart, axisEnd),
				plot_width=self.plotWidth,
				plot_height=self.plotHeight / 3)
	
			# figure2.legend.orientation = 'horizontal'
			# figure2.legend.label_text_font_size = '1pt'
			axisStart = 0.8 * min(self.df.MACD)
			axisEnd = 1.5 * max(self.df.MACD)
			# figure2.extra_y_ranges = {'MACD': Range1d(start=axisStart,end=axisEnd)}
			# figure2.add_layout(LinearAxis(y_range_name='MACD'),'right')
			figure2.toolbar.logo = None
			figure2.toolbar_location = None

			# figure 3
			figure3 = figure(
				x_axis_type='datetime',
				x_range 	=figure1.x_range,
				y_range 	=(0, 100),
				plot_width 	=self.plotWidth,
				plot_height=self.plotHeight / 3)
			figure3.toolbar.logo = None
			figure3.toolbar_location = None
			# ===============================================================================
			# Common Properties
			figure1.legend.click_policy = "hide"
			figure2.legend.click_policy = "hide"
			figure3.legend.click_policy = "hide"

			# ===============================================================================
			# creates plot according to keys list
			# figure 1
			# Creates candlestick plot
			if 'candlesticks' in keys1:
				# Creates Candlestick plot
				self.plotCandlestick(figure1)
	
			# Creates SMA plot
			for key in keys1:
				if 'SMA' in key:
					self.plotSMA(figure1, key)
			
			# Create EMA plot 
			for key in keys1:
				if 'EMA' in key:
					self.plotEMA(figure1, key)
	
			if 'volume' in keys1:
				# Creates Volume plot
				self.plotVolume(figure1)
	
			if 'trades' in keys1:
				# Creates buy/sell orders plot
				self.plotTrades(figure1)

			if 'extrema' in keys1:
				# Creates relative extrema plot
				extremaKeys = ['price', 'slowEMA', 'fastEMA', 'slowSMA', 'fastSMA']  # BEWARE: hard coding style
				self.plotExtrema_generic(p=figure1, extremaName=extremaKeys)
	
			# BEWARE - HARD CODING PRACTICE 
			# self.plotExtrema(figure1)
	
			# figure 2		
			if 'MACD' in keys2:
				self.plotMACD(figure2)
				self.plotSL(figure2)
				self.plotHist(figure2)
			
			if 'extrema' in keys2:
				# Creates relative extrema plot
				extremaKeys = ['MACD', 'Hist']  # BEWARE: hard coding style
				self.plotExtrema_generic(p=figure2, extremaName=extremaKeys)

			# figure 3
			if 'RSI' in keys3:
				self.plotRSI(figure3)
			if 'MFI' in keys3:
				self.plotMFI(figure3)

			if 'extrema' in keys3:
				# Creates relative extrema plot
				extremaKeys = ['RSI', 'MFI']  # BEWARE: hard coding style
				self.plotExtrema_generic(p=figure3, extremaName=extremaKeys)
			# ===============================================================================

			# lay = layout([figure1],
			# 			 [figure2],
			# 			 [figure3])

			lay = gridplot([figure1],
 						   [figure2],
 						   [figure3])

			# layouts.append(lay)

			tabs.append(Panel(child=lay, title=self.log[i].pair))
	
		# ===============================================================================
		# Ends loop over market-target
		
		p = Tabs(tabs=tabs)
		save(p)
		

# executes as main program
def main(args):

# 	from config2 import plotList1, plotList2, plotList3, coinPair
	from config.config import plotList1, plotList2, plotList3, coinPair
# 	from core.botlog import BotLog
	
	import os
	import datalog
	import output
	
	# import modules to use pickle deserialization
	import core.botlog as botlog
	import core.bottrade as bottrade
	import core.coinbox as coinbox

	# read input arguments
	if len(sys.argv) == 1:
# 		fileName = 'dataLog.obj'
		fileName = os.path.join(os.path.dirname(datalog.__file__), 'dataLog.obj')
	elif len(sys.argv) == 2:
# 		fileName = sys.argv[1]
		fileName = os.path.join(os.path.dirname(datalog.__file__), sys.argv[1])
	else:
		raise NotImplementedError('Invalid command line argument')

	# restore fileName object 
	fileHandler = open(fileName, 'r')
	dataLog = pickle.load(fileHandler)
	fileHandler.close()

	plot = BotPlot(dataLog)
	
	# set filename output file in absolute way and redirect to output package dir
	plot.fileName = os.path.join(os.path.dirname(output.__file__), plot.fileName) 
	
	plot.graph(coinPair,
			   plotList1,
			   plotList2,
			   plotList3)


if __name__ == '__main__':
	
	# import modules to use pickle deserialization
	import core.botlog as botlog
	import core.bottrade as bottrade
	import core.coinbox as coinbox
	
	main(sys.argv[1:])
