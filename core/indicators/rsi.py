'''
Created on Jan 31, 2018

@author: nico
'''

from botindicators import IndicatorsMethods
import core.auxf as auxf


class RSI(IndicatorsMethods):
    '''
    classdocs
    '''

    def __init__(self, key, timePeriod=False, timeUnit=False):
        self.log = '' 
        self.key = key

    def compute(self):
        priceLog = self.log.priceLog
        value = self.RSI_nb(priceLog)
        self.log.recData(self.key, value)        
