'''
Created on Jan 31, 2018

@author: nico
'''

from botindicators import IndicatorsMethods
import core.auxf as auxf


class MACD(IndicatorsMethods):
    '''
    classdocs
    '''

    def __init__(self, MACDkey, fastEMAkey, slowEMAkey, SLkey, SLtimePeriod, timeUnit, HistKey):
        self.MACDkey = MACDkey
        self.fastEMAkey = fastEMAkey
        self.slowEMAkey = slowEMAkey
        self.SLkey = SLkey
        self.SLtimePeriod = SLtimePeriod  # period in a time unit
        self.timeUnit = timeUnit
        self.HistKey = HistKey

    def assignLog(self, log):
        self.log = log
        self.computeSLperiod()

    def computeSLperiod(self):
        self.SLperiod = auxf.computePeriodInCandles(self.SLtimePeriod, self.timeUnit, self.log.period)    

    def compute(self):
        priceLog = self.log.priceLog
        self.computeMACD(priceLog)
        self.computeSL(priceLog)
        self.computeHist()
        self.computeExtremaValues(self.MACDkey)
        self.computeExtremaValues(self.HistKey)

    def computeMACD(self, priceLog):
        if len(priceLog) < 2:
            value = 0
        else:
            fastema = self.log.getPVal(self.fastEMAkey)
            slowema = self.log.getPVal(self.slowEMAkey)
            value = fastema - slowema
        self.log.recData(self.MACDkey, value)

    def computeSL(self, priceLog):
        if len(priceLog) < 2:
            value = 0
        else:
            MACDlog = self.log.getCol(self.MACDkey)
            lastSL = self.log.getLastVal(self.SLkey)
            value = self.EMA(MACDlog, self.SLperiod, lastSL)
        self.log.recData(self.SLkey, value)

    def computeHist(self):
        MACD = self.log.getPVal(self.MACDkey)
        SL = self.log.getPVal(self.SLkey)
        value = MACD - SL
        self.log.recData(self.HistKey, value)        
