'''
Created on Jan 31, 2018

@author: nico
'''

from botindicators import IndicatorsMethods
import core.auxf as auxf


class EMA(IndicatorsMethods):
    '''
    classdocs
    '''

    def __init__(self, key, timePeriod, timeUnit):
        self.key = key
        self.timePeriod = timePeriod  # period in a time unit
        self.timeUnit = timeUnit
        self.log = ''
        self.period = ''

    def assignLog(self, log):
        self.log = log
        self.computePeriod()

    def computePeriod(self):
        self.period = auxf.computePeriodInCandles(self.timePeriod, self.timeUnit, self.log.period)

    def compute(self):
        priceLog = self.log.priceLog
        if len(priceLog) < 2:
            value = 0
        else:
            lastEMA = self.log.getLastVal(self.key)
            value = self.EMA(priceLog, self.period, lastEMA)
        self.log.recData(self.key, value)
