import numpy as np
# import auxf
import core.auxf as auxf
from scipy.signal import argrelextrema


class BotIndicators(object):

    def __init__(self, log):
        self.log = log
        self.priceLog = []
        # list of indicators objects
        self.indicators = []

    def addIndicator(self, indicator):
        indicator.assignLog(self.log)
        self.indicators.append(indicator)

    def computeAll(self):
        # update priceLog 
        self.log.updatePriceLog()
        # compute all indicators
        for indicator in self.indicators:
            indicator.compute()

    def computeExtrema(self):
        # self.log.extrema --> extrema dictionary in dataLog object
        # Note: order of scipy method is considered homogeneous for all indicators
        #       this could be a source of errors

        # get extrema keys
        keys = self.log.extrema.keys() 
        for key in keys:
            keyValues = self.log.getCol(key)  # BEWARE of ignore last or not!!
            # Becareful, scipy method returns tuples, [0] --> numpy array
            maxima = argrelextrema(keyValues, np.greater, order=10)[0]
            minima = argrelextrema(keyValues, np.less, order=10)[0]
            self.log.extrema[key]['max'] = maxima
            self.log.extrema[key]['min'] = minima


class IndicatorsMethods(object):

    def __init__(self):
        raise NotImplementedError('IndicatorsMethods object should never be constructed')

    def SMA(self, dataPoints, period):
        return sum(dataPoints[-period:]) / float(len(dataPoints[-period:]))

    def momentum (self, dataPoints, period=14):
        if (len(dataPoints) > period - 1):
            return dataPoints[-1] * 100 / dataPoints[-period]

    def EMA(self, prices, period, lastEMA):
        k = 2. / float(period + 1)
        k1 = 1 - k
        ema = prices[-1] * k + lastEMA * k1
        return ema

    def MACD(self, priceLog, nfast, nslow, fastEMAlog, slowEMAlog):
        # DEPRECATED
        raise NotImplementedError('MACD indicator method is deprecated')
        # nslow should be 26
        # nfast should be 12
        if (len(priceLog) < 2):
            emafast = 0
            emaslow = 0
            macd = 0
            return emafast, emaslow, macd
        else:
            lastfastema = fastEMAlog[-1]
            emafast = self.EMA(priceLog, nfast, lastfastema)
            lastslowema = slowEMAlog[-1]
            emaslow = self.EMA(priceLog, nslow, lastslowema)
            macd = emafast - emaslow
            return emafast, emaslow, macd

    def MACDSL(self, MACDLog, period, SLLog):
        # DEPRECATED
        raise NotImplementedError('MACDSL indicator method is deprecated')
        # period should be 9
        # given a MACD history returns the signal line (SL)
        # this could be done directly by the EMA method
        if (len(MACDLog) < 2):
            return 0
        else:
            lastsl = SLLog[-1]
            signalLine = self.EMA(MACDLog, period, lastsl)
            return signalLine        

    def RSI_nb(self, prices, period=14):
        # output a neutral amount until enough prices in list to calculate RSI
        if (len(prices) < (period + 1)):
            return 50   
        # takes the last 15 prices and calculates the succesive difference between them 
        deltas = np.diff(prices[-(period + 1):])
        up = deltas[deltas >= 0].sum() / period
        down = -deltas[deltas < 0].sum() / period
        rs = up / down
        rsi = 100. - 100. / (1. + rs)
        return rsi

    def RSI (self, prices, period=14):
        deltas = np.diff(prices)
        seed = deltas[:period + 1]
        up = seed[seed >= 0].sum() / period
        down = -seed[seed < 0].sum() / period
        rs = up / down
        rsi = np.zeros_like(prices)
        rsi[:period] = 100. - 100. / (1. + rs)

        for i in range(period, len(prices)):
            delta = deltas[i - 1]  # cause the diff is 1 shorter
            if delta > 0:
                upval = delta
                downval = 0.
            else:
                upval = 0.
                downval = -delta

            up = (up * (period - 1) + upval) / period
            down = (down * (period - 1) + downval) / period
            rs = up / down
            rsi[i] = 100. - 100. / (1. + rs)
        if len(prices) > period:
            return rsi[-1]
        else:
            return 50  # output a neutral amount until enough prices in list to calculate RSI

    def MFI(self, prices, period=14):
        # output a neutral amount until enough prices in list to calculate RSI
        if (len(prices) < (period + 1)):
            return 50

        # 'Typical Price' definition
        high = self.log.data.tail(period + 1)['high'].values
        low = self.log.data.tail(period + 1)['low'].values
        close = self.log.data.tail(period + 1)['close'].values 
        TP = (high + low + close) / 3.0  

        # market volume for last period+1 candles
        # volume = self.log.getCol('mktVolume').tail(period+1)
        volume = self.log.getCol('mktVolume')[-(period + 1):]

        deltas = np.diff(TP)
        # positive Money flow
        pMF = (TP * volume)[:-1][deltas >= 0].sum()
        # negative Money flow
        nMF = (TP * volume)[:-1][deltas < 0].sum()
        # MR - Money Ratio
        MR = pMF / nMF
        # MFI - Money Flow Index
        MFI = 100. - 100. / (1. + MR)
        return MFI

    def computeExtremaValues(self, key):
        # self.log.extrema --> extrema dictionary in dataLog object
        # Note: order of scipy method is considered homogeneous for all indicators
        #       this could be a source of errors
        #
        # compute extrema values for a given datalog key
        values = self.log.getCol(key)  # Includes last value
        # ----------
        # GET INDICES
        # Becareful, scipy method returns tuples, [0] --> numpy array
        imaxima = argrelextrema(values, np.greater, order=10)[0]
        iminima = argrelextrema(values, np.less, order=10)[0]
        # print '---------'
        # print self.log.extrema
        # print '---------'
        # print self.log.extremaValues
        # print '---------'
        self.log.extrema[key]['max'] = imaxima
        self.log.extrema[key]['min'] = iminima    
        # GET VALUES
        # print type(values)
        # print values
        self.log.extremaValues[key]['max'] = values[imaxima]
        self.log.extremaValues[key]['min'] = values[iminima]
        # GET DATES
        dates = self.log.getCol('date')
        self.log.extremaDates[key]['max'] = dates[imaxima]
        self.log.extremaDates[key]['min'] = dates[iminima]

class SMA(IndicatorsMethods):

    def __init__(self, key, timePeriod, timeUnit):
        self.key = key
        self.timePeriod = timePeriod  # period in a time unit
        self.timeUnit = timeUnit
        self.log = ''
        self.period = ''

    def assignLog(self, log):
        self.log = log
        self.computePeriod()

    def computePeriod(self):
        self.period = auxf.computePeriodInCandles(self.timePeriod, self.timeUnit, self.log.period)

    def compute(self):
        priceLog = self.log.priceLog
        if len(priceLog) < self.period:
            value = 0
        else:
            value = self.SMA(priceLog, self.period)
        self.log.recData(self.key, value)


class EMA(IndicatorsMethods):

    def __init__(self, key, timePeriod, timeUnit):
        self.key = key
        self.timePeriod = timePeriod  # period in a time unit
        self.timeUnit = timeUnit
        self.log = ''
        self.period = ''

    def assignLog(self, log):
        self.log = log
        self.computePeriod()

    def computePeriod(self):
        self.period = auxf.computePeriodInCandles(self.timePeriod, self.timeUnit, self.log.period)

    def compute(self):
        priceLog = self.log.priceLog
        if len(priceLog) < 2:
            value = 0
        else:
            lastEMA = self.log.getLastVal(self.key)
            value = self.EMA(priceLog, self.period, lastEMA)
        self.log.recData(self.key, value)


class MACD(IndicatorsMethods):

    def __init__(self, MACDkey, fastEMAkey, slowEMAkey, SLkey, SLtimePeriod, timeUnit, HistKey):
        self.MACDkey = MACDkey
        self.fastEMAkey = fastEMAkey
        self.slowEMAkey = slowEMAkey
        self.SLkey = SLkey
        self.SLtimePeriod = SLtimePeriod  # period in a time unit
        self.timeUnit = timeUnit
        self.HistKey = HistKey

    def assignLog(self, log):
        self.log = log
        self.computeSLperiod()

    def computeSLperiod(self):
        self.SLperiod = auxf.computePeriodInCandles(self.SLtimePeriod, self.timeUnit, self.log.period)    

    def compute(self):
        priceLog = self.log.priceLog
        self.computeMACD(priceLog)
        self.computeSL(priceLog)
        self.computeHist()
        self.computeExtremaValues(self.MACDkey)
        self.computeExtremaValues(self.HistKey)

    def computeMACD(self, priceLog):
        if len(priceLog) < 2:
            value = 0
        else:
            fastema = self.log.getPVal(self.fastEMAkey)
            slowema = self.log.getPVal(self.slowEMAkey)
            value = fastema - slowema
        self.log.recData(self.MACDkey, value)

    def computeSL(self, priceLog):
        if len(priceLog) < 2:
            value = 0
        else:
            MACDlog = self.log.getCol(self.MACDkey)
            lastSL = self.log.getLastVal(self.SLkey)
            value = self.EMA(MACDlog, self.SLperiod, lastSL)
        self.log.recData(self.SLkey, value)

    def computeHist(self):
        MACD = self.log.getPVal(self.MACDkey)
        SL = self.log.getPVal(self.SLkey)
        value = MACD - SL
        self.log.recData(self.HistKey, value)        


class RSI(IndicatorsMethods):

    def __init__(self, key, timePeriod=False, timeUnit=False):
        self.log = '' 
        self.key = key

    def compute(self):
        priceLog = self.log.priceLog
        value = self.RSI_nb(priceLog)
        self.log.recData(self.key, value)        


class MFI(IndicatorsMethods):

    def __init__(self, key, timePeriod=False, timeUnit=False):
        self.log = ''
        self.key = key

    def compute(self):
        priceLog = self.log.priceLog
        value = self.MFI(priceLog)
        self.log.recData(self.key, value)
