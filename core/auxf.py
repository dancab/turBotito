import time
import datetime as dt

def confirmContinue():
    segui = True
    while segui:
        cont = raw_input('continue? y or n: ')
        if cont == 'n':
            return False
        elif cont == 'y':
            return True
        else:
            print 'bad input, try again'

def printandlog(msg, fileObject, topFramed=False, bottomFramed=False):
    if topFramed:
        print '=========='
        fileObject.write('==========' + '\n')
    print msg
    fileObject.write(msg + '\n')
    if bottomFramed:
        print '=========='
        fileObject.write('==========' + '\n')


def wait(fileObject=False):
    # a fuction that does nothing
    # used to wait in scheduled events
    if fileObject:
        printandlog('...waiting...', fileObject, False, False)
    else:
        print '...waiting...'


def nothingFunction():
    pass


def getCurrentLocalTimeUTS():
    return int(time.time())


def getCurrentGmTimeUTS():
    return int(time.time()) + time.timezone


def local2gm(date):
    return date + time.timezone


def gm2local(date):
    return date - time.timezone


def createTimeStamp(datestr, format="%Y-%m-%d %H:%M:%S"):
    # return time.mktime(time.strptime(datestr, format))
    return int(time.mktime(time.strptime(datestr, format)))


def createTimeString(dateuts, format='%Y-%m-%d %H:%M:%S'):
    return time.strftime(format, time.localtime(dateuts))


def uts2str(dateuts, format='%Y-%m-%d %H:%M:%S'):
    # return time.strftime(format, time.localtime(dateuts))
    return str(dt.datetime.fromtimestamp(dateuts))


def str2uts(datestr, format="%Y-%m-%d %H:%M:%S"):
    return int(time.mktime(time.strptime(datestr, format)))


def uts2stt(dateuts):
    # stt = structured time (time module format)
    # uts = unix time stamp
    return time.localtime(dateuts)


def stt2uts(datestt):
    return int(time.mktime(datestt))


def discardExcessSeconds(dateuts, candlePeriod):
    excessSeconds = dateuts % candlePeriod 
    return dateuts - excessSeconds


def computePeriodInCandles(timePeriod, timeUnit, candlePeriod):
    # converts timePeriod in timeUnit (weeks, days or hours) into a period in number of candles
    if timeUnit == 'w':
        unitVal = 7 * 24 * 3600
    elif timeUnit == 'd':
        unitVal = 24 * 3600  # a day in seconds
    elif timeUnit == 'h':
        unitVal = 3600
    elif timeUnit == 'c':
        return timePeriod  # in this case the timePeriod is given in candles already
    #
    timePeriodInSeconds = timePeriod * unitVal
    if (timePeriodInSeconds % candlePeriod) > 0:
        raise ValueError('indicator period not a multiple of the candle period')
    #
    return (timePeriodInSeconds / candlePeriod)
