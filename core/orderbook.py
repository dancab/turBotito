import pandas as pd

ORDER_TYPES = ['asks', 'bids']
ORDER_BOOK_FIELDS = ['rate', 'tgtAmount', 'tgtCoinAcum', 'mktAmount','mktCoinAcum']

def assignValue(dataframe, row, column, value):
    # assign value to dataframe cell ONLY if row and column keys exist
    if not row in dataframe.index:
        raise ValueError('row not present in dataframe: ' + str(row))
    if not column in dataframe.columns:
        raise ValueError('column not present in dataframe: ' + str(column))
    dataframe.loc[row, column] = value

def addNewRow(dataframe):
    # add a new row to a dataframe and return new row index
    index = len(dataframe)
    dataframe.loc[index] = [None for column in range(len(dataframe.columns))]
    return index

class OrderBook(object):
    def __init__(self, coinPair):
        self.coinPair = coinPair
        self.__resetOrderBook()

    @classmethod        
    def fromPoloniexOrderBook(cls, coinPair, poloniexOrderBook):
        # create empty instance
        instance = cls(coinPair)
        instance.assignPoloniexOrderBook(poloniexOrderBook)
        return instance

    def __resetOrderBook(self):
        # self.data is a dictionary of dataframes
        # one dataframe for 'asks' (sell orders)
        # one dataframe for 'bids' (buy orders)
        self.data = dict.fromkeys(ORDER_TYPES)
        for orderType in ORDER_TYPES:
            self.data[orderType] = pd.DataFrame(columns=ORDER_BOOK_FIELDS)

    def assignPoloniexOrderBook(self, poloniexOrderBook):
        # crate dataframe with columns but without rows
        self.__resetOrderBook()
        # cycle to assign
        for orderType in ORDER_TYPES:
            tgtCoinAcum = 0.0
            mktCoinACum = 0.0
            for order in poloniexOrderBook[orderType]:
                # get rate and amounts from order
                rate = float(order[0])
                tgtAmount = float(order[1])
                mktAmount = tgtAmount*rate
                tgtCoinAcum += tgtAmount
                mktCoinACum += mktAmount
                # add new row to dataframe
                newRowIndex = addNewRow(self.data[orderType])
                # assign self.data
                assignValue(self.data[orderType], newRowIndex, 'rate', rate)
                assignValue(self.data[orderType], newRowIndex, 'tgtAmount', tgtAmount)
                assignValue(self.data[orderType], newRowIndex, 'tgtCoinAcum', tgtCoinAcum)
                assignValue(self.data[orderType], newRowIndex, 'mktAmount', mktAmount)
                assignValue(self.data[orderType], newRowIndex, 'mktCoinAcum', mktCoinACum)

    def show(self):
        print '====================================='
        print 'Coin Pair: ' + self.coinPair
        print '====================================='
        print 'SELL orders (asks): '
        print self.data['asks']
        #
        print 'BUY orders (bids): '
        print self.data['bids']
        print '====================================='

    def getRateFromAmount(self, operation, amount):
        # when trying to buy certain amount
        # this method returns the potential trades with their rates and amounts
        if operation == 'buy':
            data = self.data['asks'] # data is sell orders
        elif operation == 'sell':
            data = self.data['bids'] # data is buy orders
        # get potential rate for the acumulated amount that complies with the desired amount
        iRow = next(i for i,val in enumerate(data['tgtCoinAcum'].values) if val>amount)
        rate = data.loc[iRow, 'rate']
        return rate
        
