import auxf

class Trade(object):
    # object to store poloniex's trades in a simpler manner
    def __init__(self, coinPair):
        # coin pair
        self.coinPair = coinPair
        # amount
        self.tgtAmount = 0.0
        self.mktAmount = 0.0
        # price
        self.price = 0.0
        # date
        self.date = 0.0

    @classmethod
    def fromPoloniexTrade(cls, coinPair, poloTrade):
        instance = cls(coinPair)
        instance.assignPoloniexTrade(poloTrade)
        return instance

    def assignPoloniexTrade(self, poloTrade):
        # assigns data from a trade dictionary into this trade object
        # -----------------------------------------------------------
        self.tgtAmount = float(poloTrade['amount'])
        self.mktAmount = float(poloTrade['total'])
        self.price = float(poloTrade['rate'])
        self.date = auxf.gm2local(int(auxf.str2uts(poloTrade['date'])))

    def prePrint(self):
        # prepares a message with information to be printed as the trade
        msg = 'date: ' + self.dateSTR + ' -- '  \
        + 'price: ' + str(self.price) + ' -- ' \
        + 'tgtAmount: ' +  str(self.tgtAmount)
        return msg

    def show(self):
        print '====================================='
        print 'Coin Pair: ' + str(self.coinPair)
        print 'Target Amount: ' + str(self.tgtAmount)
        print 'Market Amount: ' + str(self.mktAmount)
        print 'Price: ' + str(self.price)
        print 'Date: ' + auxf.uts2str(self.date)
        print '====================================='