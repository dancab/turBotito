import pandas as pd

def assignValue(dataframe, row, column, value):
    # assign value to dataframe cell ONLY if row and column keys exist
    if not row in dataframe.index:
        raise ValueError('row not present in dataframe')
    if not column in dataframe.columns:
        raise ValueError('column not present in dataframe')
    dataframe.loc[row, column] = value

def addNewRow(dataframe):
    # add a new row to a dataframe and return new row index
    index = len(dataframe)
    dataframe.loc[index] = [None for column in range(len(dataframe.columns))]
    return index

class Balance(object):
    def __init__(self):
        self.data = pd.DataFrame(columns=['coin', 'amount'])
        self.nzData = pd.DataFrame(columns=['coin', 'amount'])

    @classmethod
    def fromPoloniexBalance(cls, poloBalance):
        instance = cls()
        instance.assignPoloniexBalance(poloBalance)
        return instance

    def assignPoloniexBalance(self, poloBalance):
        # reset data
        self.__init__()
        # do stuff
        for coin in poloBalance:
            newRowIndex = addNewRow(self.data)
            assignValue(self.data, newRowIndex, 'coin', coin)
            assignValue(self.data, newRowIndex, 'amount', float(poloBalance[coin]))
            if float(poloBalance[coin]) >= 0.00000001:
                nzNewRowIndex = addNewRow(self.nzData)
                assignValue(self.nzData, nzNewRowIndex, 'coin', coin)
                assignValue(self.nzData, nzNewRowIndex, 'amount', float(poloBalance[coin]))

    def show(self, nonZeroOnly=True):
        if nonZeroOnly:
            data = self.nzData
        else:
            data = self.data
        print '====================================='
        print 'PERSONAL BALANCE'
        print data
        print '====================================='