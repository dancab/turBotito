import ast
import auxf


class BotChart(object):

    def __init__(self, polo, pair, period, initime='', endtime=''):
        # the key and secret should, in the future, belong to a class "account" or something like that
        self.polo = polo
        # self.polo.key = 'polo key goes here'
        # %self.polo.secret = 'polo secret goes here'

        self.pair = pair
        self.period = period

        # turboTito works witk UTC time, poloniex orders are done in local time
        if initime:
            self.startTime = initime
        if endtime:
            self.endTime = endtime

    def setDates(self, initime, endtime):
        self.startTime = initime
        self.endTime = endtime

    def getPoints(self):
        self.data = self.polo.returnChartData(self.pair, self.period, self.startTime, self.endTime)
        return self.data

    def getTrades(self, initime=False, endtime=False):
        self.trades = self.polo.marketTradeHist(self.pair, initime, endtime)
        return self.trades

    def saveFile(self, filename):
        try:
            fid = open(filename, 'w')
            fid.write('Exchange: poloniex' + '\n')
            fid.write('Coin pair: ' + str(self.pair) + '\n')
            fid.write('period: ' + str(self.period) + '\n')
            fid.write('since: ' + str(self.startTime) + ' =  ' + auxf.createTimeString(self.startTime) + '\n')
            fid.write('until: ' + str(self.endTime) + ' =  ' + auxf.createTimeString(self.endTime) + '\n')
            fid.write('data beggins at next line:' + '\n')
            for candle in self.data:
                fid.write(str(candle) + '\n')
        finally:
            fid.close()

    def loadFile(self, filename, since, until):
        # ------------
        self.data = []
        # ------------
        try:
            fid = open(filename, 'r')
            stringList = fid.read().splitlines()
        finally:
            fid.close()
        # ------------
        for s in stringList:
            # ------------
            if not s[0] == '{':
                continue
            # ------------
            else:
                # check the date to see if candle is in lapse to load
                d = ast.literal_eval(s)
                if int(d['date']) >= int(since):
                    if int(d['date']) <= int(until):
                        self.data.append(d)
                    else:
                        # end loading
                        break
            # ------------
        return self.data
        # ------------
