from auxf import confirmContinue
from trade import Trade
from order import Order
import pandas as pd
import numpy as np

class Trader(object):
    def __init__(self, communicator, coinPair):
        self.comm = communicator
        self.coinPair = coinPair
        self.mktCoin = self.coinPair.split('_')[0]
        self.tgtCoin = self.coinPair.split('_')[1]

    def changeCoinPair(self, newCoinPair):
        self.coinPair = newCoinPair
        self.mktCoin = self.coinPair.split('_')[0]
        self.tgtCoin = self.coinPair.split('_')[1]

    def select(self, operationDict):
        operationType = operationDict['Type']
        operationMethod = operationDict['Method']
        amount = float(operationDict['Amount'])
        rate = float(operationDict['Rate']) if operationDict['Rate'] else None
        limitRate = float(operationDict['Limit Rate'])
        if operationType == 'buy':
            print'buy'
            if operationMethod == 'takeOrders':
                print 'takeOrders'
                self.takeOrders('buy', amount, limitRate)
            else:
                print 'Operation Method unknown'
        elif operationType == 'sell':
            print 'sell'
            if operationMethod == 'takeOrders':
                print 'takeOrders'
                self.takeOrders('sell', amount, limitRate)
            else:
                print 'Operation Method unknown'
        else:
            print 'Operation Type unknown'


    def buyPlain(self, rate, amount, coin, orderType=False):
        print 'attempting to buy by ' + coin + 'amount'
        # -----
        if coin == self.tgtCoin:
            tgtAmount = amount
            mktAmount = tgtAmount*rate
        elif coin == self.mktCoin:
            mktAmount = amount 
            tgtAmount = amount/rate
        # -----
        print 'mktCoin amount: ' + str(amount*rate)
        print 'tgtCoin amount: ' + str(tgtAmount)
        print 'rate: ' + str(rate)
        if rate > 1.1*self.lowestAsk:
            raise ValueError('buyRate is too high, probably a typing error')
        segui = confirmContinue()
        if segui == False:
            print 'aborted operation'
            return 0
        else:
            return self.comm.buy(self.coinPair, rate, tgtAmount, orderType)

    def sellPlain(self, rate, amount, coin, orderType=False):
        print 'attempting to sell by ' + coin + ' amount'
        # -----
        if coin == self.tgtCoin:
            tgtAmount = amount
            mktAmount = tgtAmount*rate
        elif coin == self.mktCoin:
            mktAmount = amount 
            tgtAmount = amount/rate
        # -----
        print 'mktCoin amount: ' + str(amount*rate)
        print 'tgtCoin amount: ' + str(tgtAmount)
        print 'rate: ' + str(rate)
        if rate < 0.9*self.highestBid:
            raise ValueError('sell rate is too low, probably a typing error')
        segui = confirmContinue()
        if segui == False:
            print 'aborted operation'
            return 0
        else:
            return self.comm.sell(self.coinPair, rate, tgtAmount, orderType)

    def takeOrders(self, operationType, tgtAmount, limitRate, orderBook=False):
        if orderBook == False:
            print 'updating order book'
            orderBook = self.comm.getOrderBook(self.coinPair)
            orderBook.show()
            lowestAsk = orderBook.data['asks'].loc[0, 'rate']
            highestBid = orderBook.data['bids'].loc[0, 'rate']
        print 'attempting to ' + operationType + ' ' + self.tgtCoin + ' from ' + self.mktCoin
        # -----
        # =========================================
        # get row of orderBook with acumulated amount greater than wanted amount
        rate = orderBook.getRateFromAmount(operationType, tgtAmount)
        # =========================================
        # CHECK FOR ERRORS
        # check if amount is too high
        if operationType == 'buy':
            if tgtAmount > orderBook.data['asks']['tgtCoinAcum'].values[-1]:
                raise ValueError('buy amount excedes current orderBook acumulated amount, place smaller orders')
            if rate > 1.1*lowestAsk:
                raise ValueError('rate is too high, problem with rate computation')
        elif operationType == 'sell':
            if tgtAmount > orderBook.data['bids']['tgtCoinAcum'].values[-1]:
                raise ValueError('sell amount excedes current orderBook acumulated amount, place smaller orders')
            if rate < 0.9*highestBid:
                raise ValueError('rate is too low, problem with rate computation')    
        else:
            raise ValueError('orderType not buy nor sell')
        # =========================================
        # PRINT POTENTIAL TRADE
        mktAmount = tgtAmount*rate
        # printing
        print 'tgtCoin amount: ' + str(tgtAmount)
        print 'rate (aprox.): ' + str(rate)
        print 'limit rate: ' + str(limitRate)
        print 'mktCoin amount (aprox.): ' + str(tgtAmount*rate)
        # =========================================
        # CHECK FOR CONFIRMATION
        segui = confirmContinue()
        if segui == False:
            print 'aborted operation'
            return 0
        # =========================================
        # TRADE
        order = Order(operationType, self.coinPair, tgtAmount)
        if operationType == 'buy':
            orderNumber, order.trades =  self.comm.buy(self.coinPair, rate, tgtAmount, orderType='immediateOrCancel')
        elif operationType == 'sell':
            orderNumber, order.trades =  self.comm.sell(self.coinPair, rate, tgtAmount, orderType='immediateOrCancel')
        order.computeAmounts()
        order.show()
        return order
        # =========================================
