import numpy as np

class Trader(object):

    def __init__(self, log, numSimulTrades=1, umbral=0):
        self.log = log
        self.numSimulTrades = numSimulTrades
        self.umbral = umbral
        self.cpStrategies = []
        self.decision = ''

    def addCoinPairStrategy(self, cpStrategy):
        self.cpStrategies.append(cpStrategy)

    def getScores(self):
        # get evaluations for all coin pair strategies
        for cpStrat in self.cpStrategies:
            cpStrat.evaluate()

    def decide(self):
        # get coin pair strategies scores
        self.getScores()
        # check for trades currently open
        nOpenTrades = len(self.log.openTrades)
        # this is a beta version with only one strategy
        score = self.cpStrategies[0].score
        # assign a decision according to the combined score
        if nOpenTrades < self.numSimulTrades:
            pass 
        else:
            pass

        self.decision = 'STAY'

        if score > self.umbral:
            if nOpenTrades < self.numSimulTrades:
                self.decision = 'BUY'
        elif score < self.umbral:
            if nOpenTrades > 0:
                self.decision = 'SELL'


class CoinPairStrategy(object):

    def __init__(self, strategy='none', coinPair=''):
        self.strategy = strategy
        self.coinPair = coinPair
        self.strategies = []
        self.weights = []
        self.score = 0

    def addStrategy(self, strategy, weight):
        self.strategies.append(strategy)
        self.weights.append(weight)

    def getScores(self):
    # get decisions for all strategies
        for strategy in self.strategies:
            strategy.evaluate()

    def evaluate(self):
        # get decisions from strategies 
        self.getScores()
        # select the master strategy
        if self.strategy is 'someStrategy':
            self.someStrategy()
        elif self.strategy is 'anotherOne':
            self.anotherOne()
        else:
            exit('wrong master strategy selection')

    def someStrategy(self):
        # initialize scores
        self.score = 0
        # create an uniform distribution for weigths
        n = len(self.strategies)  # number of strategies
        self.weights = [1.0 / n for i in range(n)]
        # calculate weighted arithmetic mean (weights add up to 1)
        activeWeights = self.weights
        for i in range(n):
            if self.strategies[i].active:
                self.score += self.weights[i] * self.strategies[i].score
            else:
                del activeWeights[i]
        self.score = self.score / float(sum(activeWeights))

    def anotherOne(self):
        pass


class BotStrategy(object):

    def __init__(self, log, umbral=0, strategy='none'):
        # log is an object where information is stored
        self.log = log
        self.umbral = umbral
        self.strategy = strategy
        self.score = ''  # score defined in [-1,1] range
        self.active = True

    def evaluate(self):
        if self.strategy is 'simpleSMA':
            self.simpleSMA()
        elif self.strategy is 'simpleSMAder':
            self.simpleSMAder()
        elif self.strategy is 'simpleMACD':
            self.simpleMACD()
        elif self.strategy is 'dpMACD':
            self.dpMACD()
        elif self.strategy is 'MACD_bis':
            self.MACD_bis()
        elif self.strategy is 'simpleRSI':
            self.simpleRSI()
        else:
            exit('wrong strategy selection')

    def simpleSMA(self):
        numOpenTrades = len(self.log.openTrades)
        price = self.log.getPVal('price')
        SMA15 = self.log.getPVal('SMA15')

        self.score = 0  # do nothing

        if SMA15 - price > self.umbral:
            self.score = 1  # favor target currency
        if SMA15 - price < self.umbral:
            self.score = -1  # favor market currency

    def simpleMACD(self):
        numOpenTrades = len(self.log.openTrades)
        price = self.log.getPVal('price')
        MACD = self.log.getPVal('MACD')

        self.score = 0  # do nothing

        if MACD > self.umbral:
            self.score = 1  # favor target currency
        if MACD < self.umbral:
            self.score = -1  # favor market currency

    def dpMACD(self):
        # double positive MACD
        MACD = self.log.getPVal('MACD')
        SL = self.log.getPVal('SL')
        HIST = self.log.getPVal('Hist')
        # reference value
        maxValsMACD = self.log.extremaValues['MACD']['max']
        minValsMACD = self.log.extremaValues['MACD']['min']
        maxValsHist = self.log.extremaValues['Hist']['max']
        minValsHist = self.log.extremaValues['Hist']['min']
        valuesMACD = np.abs(np.concatenate((maxValsMACD, minValsMACD)))
        valuesHist = np.abs(np.concatenate((maxValsHist, minValsHist)))
        n = 2
        if len(valuesMACD)>n:
            valuesMACD = valuesMACD[-n:]
        if len(valuesHist)>n:
            valuesHist = valuesHist[-n:]
        refValMACD = np.max(valuesMACD)
        refValHist = np.max(valuesHist)

        self.score = 0  # do nothing

        umbralMACD = 0
        umbralHist = 0
        # print maxminabsvalues
        # print  'refVal: ' + str(refVal) + ' --  self.umbral: ' + str(self.umbral) + ' -- umbral: ' + str(umbral) 

        if MACD > refValMACD and HIST > refValHist:
            self.score = 1 # favor target currency
        elif MACD < refValMACD or HIST < -refValHist:
            self.score = -1 # favor market currency

    def sauMACD(self):
        # double positive MACD
        EMA12 = self.log.getPVal('fastEMA')
        MACD = self.log.getPVal('MACD')
        SL = self.log.getPVal('SL')
        HIST = MACD - SL

        self.score = 0  # do nothing

        if SL < 0 and HIST > 0:
            self.score = 1  # favor target currency

        if  HIST < 0:
            self.score = -1  # favor market currency

    def MACD_bis(self):
        # double positive MACD
        numOpenTrades = len(self.log.openTrades)
        price = self.log.getPVal('price')
        MACD = self.log.getPVal('MACD')
        SL = self.log.getPVal('SL')
        # access to Log dataframe
        # access to MACD - SL historical data 
        MACD_SL = (self.log.data['MACD'] - self.log.data['SL']).values

        self.score = 0  # do nothing 
        
        tol = 0.80  # relative measure

        if MACD_SL[-1] > 0:
            if numOpenTrades is 0:
                self.score = 1
            # positive scheme for MACD-SL 
            for i in range(len(MACD_SL)):
                if MACD_SL[-i] < 0:
                    break
            values = MACD_SL[-i + 1:]
            maxVal = np.amax(values)
            diffValues = np.diff(values)
    
            # ! MACD-SL POSITIVE SCHEME
            # ! maxVal possitive ensures a MACD-SL positive scheme
            # ! (values[-1] + diffValues[-1])/maxVal > 0 indicates that extrapolation
            #   stays on positive scheme for MACD-SL

            if (
                maxVal > 0                and 
                values[-1] / maxVal < tol and
                diffValues[-1] < 0 and
                diffValues[-2] < 0 and
                numOpenTrades > 0 
                # (values[-1]-diffValues[-1])/maxVal > 0
                ):
                self.score = -1
                
        elif MACD_SL[-1] < 0:
            # negative scheme for MACD-SL
            for i in range(len(MACD_SL)):
                if MACD_SL[-i] > 0:
                    break
            values = MACD_SL[-i + 1:]
            minVal = np.amin(values)
            maxVal = abs(minVal)
            diffValues = np.diff(values)

            if (
                minVal < 0                   and
                abs(values[-1]) / maxVal < tol and
                numOpenTrades == 0
                ):
                self.score = 1

        # print 'indicator = ', abs(values[-1])/maxVal, 

    def simpleRSI(self):
        numOpenTrades = len(self.log.openTrades)
        price = self.log.getPVal('price')
        RSI = self.log.getPVal('RSI')
        SMA15 = self.log.getPVal('SMA15')

        self.score = 0  # do nothing

        RSIlowerBand = 20
        RSIupperBand = 80 
        if abs(RSI / RSIlowerBand - 1) < 0.15 and price > SMA15 * 1.01:
            self.score = 1  # favor target currency
        if abs(RSI / RSIupperBand - 1) < 0.15 and price < SMA15 * 0.99:
            self.score = -1  # favor market currency
