# ==============================================================================
TRADE HISTORY
Poloniex's API has two methods called 'returnTradeHistory'. 
One is public and one private.
To avoid confussion the wrapper poloniex_full changes the name of the public command.
marketTradeHist method returns the public trade history for a given pair.
# ==============================================================================

# ==============================================================================
CURRENCY PAIR INCONSISTENCE

forex currency pair format:

XXX/YYY

XXX: base currency
YYY: quote curreny
price: 1 XXX = price*YYY

poloniex currency pair format:

BTC/USDT

price = 4000  => 1 BTC = 4000 USDT
BTC: base
USDT: volume

As it is shown next, 'quoteVolume' is actually the base volume. And viceversa.

quoteVolume = 3000 BTC
(base)volume = 12000000 USDT
# ==============================================================================
