from core import auxf as auxf

# ===============================================================================
# CONTROL PARAMETERS
# --------------------------------------
initBalance = {'BTC': 1}
# coinPair    = ['BTC_ETH','BTC_XMR']
# coinPair    = ['BTC_CVC', 'BTC_XVC', 'BTC_PINK', 'BTC_SYS', 'BTC_EMC2', 'BTC_RADS', 'BTC_SC', 'BTC_MAID', 'BTC_BCH', 'BTC_GNT', 'BTC_BCN', 'BTC_REP', 'BTC_BCY', 'BTC_GNO', 'BTC_FCT', 'BTC_GAS', 'BTC_LBC', 'BTC_DCR', 'BTC_AMP', 'BTC_XPM', 'BTC_NXT', 'BTC_VTC', 'BTC_PASC', 'BTC_GRC', 'BTC_NXC', 'BTC_BTCD', 'BTC_LTC', 'BTC_DASH', 'BTC_STORJ', 'BTC_ZEC', 'BTC_BURST', 'BTC_BELA', 'BTC_STEEM', 'BTC_ETC', 'BTC_ETH', 'BTC_HUC', 'BTC_STRAT', 'BTC_LSK', 'BTC_EXP', 'BTC_CLAM', 'BTC_ZRX', 'BTC_BLK', 'BTC_XRP', 'BTC_NEOS', 'BTC_OMG', 'BTC_BTS', 'BTC_DOGE', 'BTC_SBD', 'BTC_XCP', 'BTC_BTM', 'BTC_OMNI', 'BTC_NAV', 'BTC_FLDC', 'BTC_XBC', 'BTC_DGB', 'BTC_VRC', 'BTC_RIC', 'BTC_STR', 'BTC_POT', 'BTC_XMR', 'BTC_VIA', 'BTC_XEM', 'BTC_NMC', 'BTC_ARDR', 'BTC_FLO', 'BTC_GAME', 'BTC_PPC']
coinPair = ['BTC_BCH', 'BTC_GNT', 'BTC_BCN', 'BTC_NXT', 'BTC_LTC', 'BTC_DASH', 'BTC_ZEC', 'BTC_ETC', 'BTC_ETH', 'BTC_STRAT', 'BTC_XRP', 'BTC_NEOS', 'BTC_OMG', 'BTC_DOGE', 'BTC_STR', 'BTC_XMR', 'BTC_XEM']
size 		 = len(coinPair)
candlePeriod = [900 for i in range(size)]
# --------------------------------------
fromFile = False
chartDataFile = 'chartData.log'
# --------------------------------------
# backtest dates
now = auxf.getCurrentLocalTimeUTS()
since = now
# since = auxf.createTimeStamp('2017-10-11 09:00:01')
# until = auxf.createTimeStamp('2017-10-12 09:00:01')

# --------------------------------------
# list of indicators to compute and to show on screen and to plot
indicatorsList = []
indicatorsList.append(dict(tipo='EMA', key='fastEMA', timePeriod=12, timeUnit='c'))
indicatorsList.append(dict(tipo='EMA', key='slowEMA', timePeriod=26, timeUnit='c'))
indicatorsList.append(dict(tipo='RSI', key='RSI'	 , timePeriod=14, timeUnit='c'))
indicatorsList.append(dict(tipo='MACD', MACDkey='MACD', fastEMAkey='fastEMA', slowEMAkey='slowEMA', SLkey='SL', SLtimePeriod=9, timeUnit='c', HistKey='Hist'))
indicatorKeys = ['RSI', 'fastEMA', 'slowEMA', 'MACD', 'SL', 'Hist']
# 
showList 	 = ['price', 'fastEMA', 'slowEMA', 'MACD', 'SL', 'Hist']
# plotList1 	   = ['candlesticks', 'SMA5', 'trades', 'volume', 'EMA12', 'EMA26']
plotList1 = ['candlesticks', 'trades', 'volume', 'fastEMA', 'slowEMA', 'extrema']
plotList2 	 = ['MACD', 'extrema']
# plotList2 	   = ['MACD']
# plotList3 	   = ['RSI','MFI']
plotList3 = ['DoNothing']
# --------------------------------------
# strategy parameters
stratUmbral = [0.0000 for i in range(size)]
stratKey = ['dpMACD' for i in range(size)]
stratWeight = [1 for i in range(size)]
# coinPair strategy parameters
cpStratKey = ['someStrategy' for i in range(size)]
# --------------------------------------
# trader parameters
maxSimulTrades = [1 for i in range(size)]
traderUmbral = [0 for i in range(size)]
# --------------------------------------
# backlogging parameters
nBacklogCandles = [100 for i in range(size)]  # BEWARE if you change it. Used in BotPlot
# --------------------------------------
# trades fees
# still to implement
# ===============================================================================

