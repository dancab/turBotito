'''
Created on Feb 5, 2018

@author: nico
'''
import os

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart 

import imaplib
import email


# to load keys in json format
import json
# to use config package directory (credentials are stored here)
import plugins


class emailIO(object):
    """
    Class to send/receive emails.
    Log credentials are stored in default notifierKeys.json file
    Every time it sends/receives an email, stablish connection
    and close when finalize in order to avoid connection lossses
    when not used.

    PUBLIC METHODS:
        # getEmail
        # sendEmail

    PRIVATE METHODs:
        # __stablishIMAPConnection
        # __stablishSMTPConnection
    """
    def __init__(self,fileName='notifierKeys.json'):

        try:
            fileName = os.path.join(os.path.dirname(plugins.__file__),fileName)
            fileHandler = open(fileName,'r')
            credentials = json.load(fileHandler)
    
            # email account credentials
            self.userAccount  = credentials['email']['user']
            self.userPassword = credentials['email']['pass']

        except:
            raise ValueError('There is no credentials to initialize the notifier')

    def __stablishIMAPConnection(self):


        self.readServer = imaplib.IMAP4_SSL('imap.gmail.com', 993)
        self.readServer.login(self.userAccount, self.userPassword)

        # adds readonly for security purposes
        mailbox = "INBOX"
        self.readServer.select(mailbox, 'readonly')

    def __stablishSMTPConnection(self):

        self.sendServer = smtplib.SMTP('smtp.gmail.com', 587)
        self.sendServer.starttls()
        self.sendServer.login(self.userAccount, self.userPassword)


    def getEmail(self, emailID):
#         """
#         gets and email for a given emailID.
#         emailID's are not easy to find. It is necessary to develop more
#         methods (private maybe) to find them.
# 
#         This method return a dict with the following keys:
#             'Body'     :
#             'Subject'  :
#             'From'     :
#             'To'       :
#             'Reply-To' :
#         """

        msg = {}

        self.__stablishIMAPConnection()
        stat, data = self.readServer.fetch(emailID,'(RFC822)')
        self.readServer.close()
        self.readServer.logout()

        if stat=='OK':
            xxx = data[0][1]
            xyz = email.message_from_string(xxx)
            if xyz.get_content_maintype() == 'multipart':
                for part in xyz.walk():
                    if part.get_content_type() == 'text/plain':
                        body = part.get_payload(decode=True)
                    else:
                        continue

        msg['Body']          = body
        msg['Subject']    = xyz['Subject']
        msg['From']         = xyz['From']
        msg['To']           = xyz['To']
        msg['Reply-To']   = xyz['Reply-To']

        return msg


    def sendEmail(self,toList,subject,message):
#          """
#          sends an email to toList destinaries, with subject and 
#          message body 'message'
#          
#          INPUT:
#              toList: List of strings
#              subject: string
#              message: string 
#          Note that multiline message could be sent using '\n' break
#          line character.
#          """
        toListString = ",".join(toList)

        # allow either one attachment as string, or multiple as list
        # if not isinstance(attach,list):
        #     attach = [attach]
        msg             = MIMEMultipart()
        msg['From']        = self.userAccount
        msg['To']        = toListString
        msg['Subject']    = subject
        body            = message

        msg.attach(MIMEText(body,'plain'))
        text = msg.as_string()

        self.__stablishSMTPConnection()
        self.sendServer.sendmail(self.userAccount,toListString,text)
        self.sendServer.quit()


def writeEmail(toList,subject,message):
    '''
    minimal function to write an email.
    INPUT:
    toList : List of string
    subject: string
    message: string
    '''
    email = emailIO()
    email.sendEmail(toList,subject,message)




