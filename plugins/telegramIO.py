'''
Created on Feb 22, 2018

@author: nico
'''

import telebot

import os

# to load keys in json format
import json
# to use config package directory (credentials are stored here)
import plugins

def writeTelegram(chatID,message):
    '''
    function to send message to specific user (chatId)
    
    NOTA: Es probable que no funcione junto con el bot activo (problema con
    el token)
    '''
    #==========================================================================
    # reading credentials
    fileName = 'notifierKeys.json'                                     
    fileName = os.path.join(os.path.dirname(plugins.__file__),fileName)
    fileHandler = open(fileName,'r')                                   
    credentials = json.load(fileHandler)                               
                                                                       
    # Telegram account credentials                                     
    telegramToken  = credentials['telegram']['token']                  
    #==========================================================================
    
    bot = telebot.TeleBot(telegramToken)
    
    bot.send_message(chatID,message)
    
    
    
    
    
