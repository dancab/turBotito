'''
Created on Feb 6, 2018

@author: nico
'''
import os

# to load keys in json format
import json
# to use config package directory (credentials are stored here)
import plugins

# bad practice
from twitter import *


class twitterIO(object):
    """
    Class to send/receive emails and post status on twitter.
    Log credentials are stored in default notifierKeys.json file
    Every time it sends/receives an emailIO, stablish connection
    and close when finalize in order to avoid connection lossses
    when not used.
    Similar when post status on Twitter.

    PUBLIC METHODS:
        # postStatusTwitter
    """

    def __init__(self, fileName='notifierKeys.json'):

        try:
            fileName = os.path.join(os.path.dirname(plugins.__file__), fileName)
            fileHandler = open(fileName, 'r')
            credentials = json.load(fileHandler)

            # twitter account credentials
            self.consumerKey = credentials['twitter']['consumer_key'] 
            self.consurmerSecret = credentials['twitter']['consumer_secret']
            self.accessKey = credentials['twitter']['access_key']
            self.accessSecret = credentials['twitter']['access_secret'] 

        except:
            raise ValueError('There is no credentials to initialize the notifier')

    def postStatusTwitter(self, newStatus):
        
        # create twitter API object
        twitter = Twitter(auth=OAuth(self.accessKey, self.accessSecret, self.consurmerKey, self.consurmerSecret))

        # post a new status
        results = twitter.statuses.update(status=newStatus)


def writeTwitter(status):
    '''
    minimal function to write a Twitter status
    INPUT:
    status: string
    '''
    
    twitter = twitterIO()
    twitter.postStatusTwitter(status)  

