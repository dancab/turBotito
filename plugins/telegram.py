'''
Created on Feb 21, 2018
Telegram Bot
name: @TurboTelegramBot

@author: nico
'''
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

# to load keys in json format
import json
# to use config package directory (credentials are stored here)
import plugins

import telebot


#==============================================================================
# reading credentials
try:
    fileName = 'notifierKeys.json'                                                                      
    fileName = os.path.join(os.path.dirname(plugins.__file__),fileName)   
    fileHandler = open(fileName,'r')                                      
    credentials = json.load(fileHandler)                                  
                                                                          
    # Telegram account credentials                                           
    telegramToken  = credentials['telegram']['token']                      
except:                                                                   
    raise ValueError('There is no credentials to initialize Telegram Bot')
#==============================================================================

bot = telebot.TeleBot(telegramToken)

@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    bot.reply_to(message, "Howdy, how are you doing?")

@bot.message_handler(commands=['hello'])
def send_helloWorld(message):
    bot.reply_to(message, "Hello World")

@bot.message_handler(commands=['getid'])
def getId(message):
    
    chatId_nb     = 401369448
    chatId_dancab = 447636237
    
    chatId       = message.chat.id
    chatUserName = message.chat.username
    
    replyMessage  = 'Hi ' + '@'+str(chatUserName) + ', your Id is ' + str(chatId)
    bot.reply_to(message, replyMessage)
    bot.send_message(chatId,'forwarding to bot developer')
    
    bot.send_message(chatId_nb,
                     text = 'fwd: ' + replyMessage)


@bot.message_handler(func=lambda message: True)
def echo_all(message):
    bot.reply_to(message, message.text)

bot.polling()