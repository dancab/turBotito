import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from account import Account
from poloniexcomm import PoloniexComm
from trader import Trader
from configfile import ConfigFile, SECTION_KEYS
from auxf import confirmContinue
import time

def display_title_bar(subtitle=False):
    # Clears the terminal screen, and displays a title bar.
    os.system('clear')
              
    print "\t**********************************************"
    print "\t***            AltCoin Trader              ***"
    print "\t**********************************************"
    if subtitle == False:
        return 0
    print "\t**********************************************"
    print "\t                " + subtitle
    print "\t**********************************************"

def menu_main_display():
    # Let users know what they can do.
    print 
    print '[1] Retrieve Menu.'
    print '[2] Set Menu'
    print '[3] Trade Menu'
    print '[r] Reload configuration'
    print '[q] Quit.'
    return raw_input('What would you like to do? ')

def menu_main():
    choice = ''
    display_title_bar('MAIN MENU')
    while choice != 'q':
        choice = menu_main_display()
        display_title_bar('MAIN MENU')
        if choice == '1':
            menu_retrieve()
        elif choice == '2':
            menu_set()
        elif choice == '3':
            menu_trade()
        elif choice == 'r':
            configFile.readFile()
            newCoinPair = '_'.join((configFile.data['Market']['Market Coin'], configFile.data['Market']['Target Coin']))
            trader.changeCoinPair(newCoinPair)
            configFile.printData()
        elif choice == 'q':
            quit()
        else:
            print("\nI didn't understand that choice.\n")

def menu_retrieve_display():
    # Let users know what they can do.
    print 
    print '[1] Retrieve personal balance.'
    print '[2] Retrieve open orders'
    print '[3] Retrieve exchange order book'
    print '[b] Go back.'    
    return raw_input('What would you like to do? ')

def menu_retrieve():
    choice = ''
    display_title_bar('RETRIEVE MENU')
    while choice != 'b':    
        choice = menu_retrieve_display()
        display_title_bar('RETRIEVE MENU')
        if choice == '1':
            # get my balance
            myBalance = communicator.getBalances()
            myBalance.show()
        elif choice == '2':
            # get my open orders
            openOrders = communicator.getOpenOrders(coinPair)
            if openOrders:
                print openOrders
            else:
                print 'No open orders found'
        elif choice == '3':
            # get current order book
            cpOrderBook = communicator.getOrderBook(trader.coinPair)
            cpOrderBook.show()
        elif choice == 'b':
            display_title_bar('MAIN MENU')
            return 0
        else:
            print("\nI didn't understand that choice.\n")

def menu_trade_display():
    print 
    print '[1] Take Orders'
    print '[b] Return to Main Menu'
    return raw_input('What would you like to do? ')

def menu_trade():
    choice = ''
    display_title_bar('TRADE MENU')
    # get my balance
    myBalance = communicator.getBalances()
    myBalance.show()
    while choice != 'b':
        choice = menu_trade_display()
        display_title_bar('TRADE MENU')
        if choice == '1':
            trader.select(configFile.data['Operation'])
        elif choice == 'b':
            display_title_bar('MAIN MENU')
            return 0
        else:
            print("\nI didn't understand that choice.\n")

def menu_set_display():
    print
    print 'SET MENU'
    print 
    # Let users know what they can do.
    print '[1] Set Market Coin.'
    print '[2] Set Target Coin'
    print '[n] Set parameter by name'
    print '[b] Go back.'    
    return raw_input('What would you like to do? ')

def menu_set():
    choice = ''
    configFile.printData()
    while choice != 'b':
        display_title_bar('SET MENU')
        configFile.printData()
        choice = menu_set_display()
        if choice == '1':
            # set market coin
            newMktCoin = raw_input('Enter new Market Coin: ')
            configFile.setParameter('Market', 'Market Coin', newMktCoin)
        elif choice == '2':
            # set market coin
            newTgtCoin = raw_input('Enter new Target Coin: ')
            configFile.setParameter('Market', 'Target Coin', newTgtCoin)
        elif choice == 'n':
            # set market coin
            secKey = raw_input('Enter section: ')
            parKey = raw_input('Enter parameter key to modify: ')
            parVal = raw_input('Enter new parameter value: ')
            ierr = configFile.setParameter(secKey, parKey, parVal)
            if ierr == 1:
                print 'Error in input data - parameter not set'
                time.sleep(1)
        elif choice == 'b':
            display_title_bar('MAIN MENU')
            return 0
        else:
            print("\nI didn't understand that choice.\n")

def quit():
    choice = ''
    while True:
        display_title_bar('EXIT MENU')
        choice = raw_input('Do you want to save configuration into configuration file? y or n ')
        if choice == 'y':
            configFile.writeFile()
            break
        elif choice == 'n':
            pass
            break
        else:
            print("\nI didn't understand that choice.\n")        
    print 'Thanks for playing. Bye!'
    time.sleep(1)
    os.system('clear')

# configuration file
display_title_bar()
configFileStr = 'traderApp.config'
configFileStr = os.path.join(os.path.dirname(output.__file__), configFileStr)
configFile = ConfigFile(configFileStr)
configFile.readFile()
if configFile.status == 'blank':
    print 'NO CONFIGURATION FILE'
    time.sleep(1)
    exit()

# set account
exchange = configFile.data['Exchange']['Exchange Name']
key = configFile.data['Exchange']['Key']
secret = configFile.data['Exchange']['Secret']
cuenta = Account(exchange, key, secret)
# set market
mktCoin = configFile.data['Market']['Market Coin']
tgtCoin = configFile.data['Market']['Target Coin']
coinPair = '_'.join((mktCoin, tgtCoin))
# create communicator with poloniex
communicator = PoloniexComm(cuenta)
# create trader
trader = Trader(communicator, coinPair)
# go to main menu
menu_main()