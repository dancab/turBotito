import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
# ===============================================================================
# HEADER
# ===============================================================================
# python modules
import sched
import time
import datetime as dt
import pickle

# botito package
import output
# botito modules
# from botchart import BotChart
from core.botchart import BotChart
# from candlestick import CandleStick
from core.candlestick import CandleStick
# from bottrade import BotTrade, Trade
from core.bottrade import BotTrade
from core.trade import Trade
# from botlog import BotLog
from core.botlog import BotLog
# from botindicators import BotIndicators, SMA, EMA, MACD, RSI, MFI
from core.indicators.botindicators import BotIndicators, SMA, EMA, MACD, RSI, MFI
# la linea de arriba con todos los indicadores es valida porque estan duplicados
# from core.indicators.ema import EMA
# from core.indicators.macd import MACD
# from core.indicators.sma import SMA
# from core.indicators.rsi import RSI 
# from core.indicators.mfi import MFI

# from botstrategy import BotStrategy, CoinPairStrategy, Trader
from core.strategies.botstrategy import BotStrategy, CoinPairStrategy, Trader
# from coinbox import CoinBox
from core.coinbox import CoinBox
# ploniex module
# from poloniex_full import Poloniex
from core.poloniex.poloniex_full import Poloniex
# auxiliar module
# import auxf
import core.auxf as auxf
# from auxf import printandlog, nothingFunction
from core.auxf import printandlog, nothingFunction
# ===============================================================================

# ===============================================================================
# CONTROL PARAMETERS
# from config import *
from config.config import *
# ===============================================================================

# ===============================================================================
# OBJECTS CONSTRUCTION
# ===============================================================================
# create a coinbox to maintain a balance
# BEWARE OF THISSSSS                    
coinbox = CoinBox(initBalance)          
print 'Balance: ', coinbox.balance      
del coinbox
# ===============================================================================
# create Poloniex object (communicator)
polo = Poloniex()
# ===============================================================================
# object list definitions
coinbox = []
chart = []
dataLog = []
indicators = []
strat = []
cpStrat = []
trader = []
candle = []
trade = []

for i in range(size):
    # create coinbox to mantain a balance (independent)
    coinbox.append(CoinBox(initBalance))
    # ===============================================================================
    # create chart object 
    chart.append(BotChart(polo, coinPair[i], candlePeriod[i]))
    # ===============================================================================
    # create log object
    dataLog.append(BotLog(coinPair[i], candlePeriod[i], nBacklogCandles[i]))
    # initialize keys to avoid logging errors
    dataLog[i].initData(indicatorKeys)
    # ===============================================================================    
    # create indicators objects
    indicators.append(BotIndicators(dataLog[i]))
    for indicatorDict in indicatorsList:
        indicatorDict_copy = indicatorDict.copy()
        tipo = indicatorDict_copy['tipo']
        print tipo
        del indicatorDict_copy['tipo']
        if tipo == 'SMA':
            indicator = SMA(**indicatorDict_copy)
        elif tipo == 'EMA':
            indicator = EMA(**indicatorDict_copy)
        elif tipo == 'MACD':
            print indicatorDict_copy
            indicator = MACD(**indicatorDict_copy)
        indicators[i].addIndicator(indicator)
    # ===============================================================================
    # create strategy object
    strat.append(BotStrategy(dataLog[i], stratUmbral[i], stratKey[i]))
    # ===============================================================================
    # create coin pair strategy object
    # strategies could change if strat = [strat,strat2,strat3] (for example, NOW)
    cpStrat.append(CoinPairStrategy(strategy=cpStratKey[i], coinPair=coinPair[i]))
    # add strategies to the master strategy
    cpStrat[i].addStrategy(strat[i], weight=stratWeight[i])
    # ===============================================================================
    # create trader object
    trader.append(Trader(dataLog[i], maxSimulTrades[i], traderUmbral[i]))
    # add master strategies to trader
    trader[i].addCoinPairStrategy(cpStrat[i])
    # ===============================================================================
    # create candlestick object to arrange data
    # will later be used to assemble candlesticks from ticker data
    candle.append(CandleStick())
    # ===============================================================================

# =============================================================================== 
# scheduler object
scheduler = sched.scheduler(time.time, time.sleep)
# ===============================================================================
# logfile to save data
logfileName = 'turBotito.log'
logfileName = os.path.join(os.path.dirname(output.__file__), logfileName)
logfileObject = open(logfileName, 'w')
# ===============================================================================

# ===============================================================================
# DATES
# ===============================================================================
now = dt.datetime.today()
oneSecond = dt.timedelta(seconds=1)
# ----
since = now

since = dt.datetime(year=2017, month=12, day=12)
since_uts = auxf.stt2uts(since.timetuple()) # microseconds are left behind
until = dt.datetime(year=2017, month=12, day=12, hour=2) - oneSecond
until_uts = auxf.stt2uts(until.timetuple()) # microseconds are left behind

# ===============================================================================
# first prepare the initial time to match a rounded time
printandlog('since:   ' + str(since), logfileObject, True, False)
since_uts = auxf.discardExcessSeconds(since_uts, candlePeriod[0])
since = dt.datetime.fromtimestamp(since_uts)
printandlog('since: ' + str(since), logfileObject, False, True)
# ===============================================================================

# ===============================================================================
# BACKLOGGING: first chart data is for filling indicators information
for i in range(size):
    sinceBL_uts = since_uts - nBacklogCandles[i] * candlePeriod[i]
    untilBL_uts = since_uts - 1 
    chartBL = BotChart(polo, coinPair[i], candlePeriod[i], initime=sinceBL_uts, endtime=untilBL_uts)
    chartDataBL = chartBL.getPoints()
    # printandlog('Backlogging', logfileObject, True, True)
    # printandlog('sinceBL: ' + str(dt.datetime.fromtimestamp(sinceBL_uts)) + ' ' +'untilBL: ' + str(dt.datetime.fromtimestamp(untilBL_uts)), logfileObject, True, True)
    for candlestick in chartDataBL:
        # rearrange data
        candle[i].resetCandle()
        candle[i].assignData(candlestick)
        # create space in log for new data
        dataLog[i].addNewRow()
        # fill data in log
        dataLog[i].recCandlestick(candle[i])
        # compute and log indicators
        indicators[i].computeAll()
        # show present time   
        # printandlog(dataLog.prePrintPRow(showList), logfileObject, False, True)
        # dataLog.showPRow(keys=['price', 'SMA15','EMA12', 'EMA26', 'MACD'])
        dataLog[i].showPRow(showList)
    print 'END Backlogging' + str('\t') + str(coinPair[i])
# ===============================================================================

# ===============================================================================
# first candle initialization
candleStartTime_uts = since_uts - candlePeriod[0]
# ===============================================================================

# ===============================================================================
nCandles = 0
segui = True
while segui:
    nCandles = nCandles + 1

    candleStartTime_uts = candleStartTime_uts + candlePeriod[0]
    candleEndTime_uts = candleStartTime_uts + candlePeriod[0] - 1
    scheduler.enterabs(candleEndTime_uts + 5, 1, nothingFunction, ())
    scheduler.run()

    # ===============================================================================
    # assembling candles
    for i in range(size):
        tradesData = chart[i].getTrades(candleStartTime_uts, candleEndTime_uts - 1)
        candle[i].resetCandle()
        candle[i].assignTimes(candleStartTime_uts, candlePeriod[0])
        for poloTrade in tradesData:
            trade = Trade(coinPair[i])
            trade.assignPoloniexTrade(poloTrade)
            candle[i].addTrade(trade)
        printandlog('assembling ' + coinPair[i], logfileObject, False, False)
        candle[i].assemble()
    # ===============================================================================

    # for debugging purposes
    for i in range(size):
        printandlog(candle[i].prePrint(), logfileObject, True, False)

    for i in range(size):
        # ===============================================================================
        # create space in log for new data
        dataLog[i].addNewRow()
        # fill candlestick data in log
        dataLog[i].recCandlestick(candle[i])
        # compute and log indicators
        indicators[i].computeAll()
        # ===============================================================================    

        # ===============================================================================
        # call in the trader to make a decision
        # the trader calls in all coin pair strategies 
        # the coin pair strategies call in the simple indicators strategies
        # -
        # the result is a decision over a given set of coin pair
        # based on several indicators strategies for each coin pair
        # -
        trader[i].decide()
        # ===============================================================================
        decision = trader[i].decision
        if decision is 'BUY':
            # create new trade object
            newTrade = BotTrade(coinbox[i], mktCurrency=coinPair[i].split('_')[0], tgtCurrency=coinPair[i].split('_')[1])
            # BEWARE OF NEXT LINE, fixed at BTC because initBalance is fixed
            newTrade.openTrade(candle[i].price, 0.002, coinbox[i].balance['BTC'], candle[i].date)
            # newTrade.showTrade()
            # print 'Balance: ', coinbox[i].balance #
            # record trade in the log
            dataLog[i].addTrade(newTrade)
        elif decision is 'SELL':
            for tradeIP, trade in enumerate(dataLog[i].openTrades):
                trade.closeTrade(candle[i].price, 0.002, candle[i].date)
                # trade.showTrade() #
                # print 'Balance: ', coinbox[i].balance #
                # change trade in the log
                dataLog[i].rmTrade(tradeIP, trade)
        elif decision is 'STAY':
            pass
        else:
            exit('ERROR')
    # ===============================================================================

    # ===============================================================================
    # save DataLog
    fileName = os.path.join(os.path.dirname(output.__file__), 'dataLog.obj')
    fileHandler = open(fileName, 'w')
    pickle.dump(dataLog, fileHandler)
    fileHandler.close()
    # ===============================================================================
    if candleEndTime_uts + 1 >= until_uts:
        printandlog('FINISH AT: ' + auxf.uts2str(candleEndTime_uts) + ' = ' + str(candleEndTime_uts), logfileObject, True, True)
        segui = False
