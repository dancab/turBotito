updated on February 24th, 2018

### Core changes

* Simplify trades data strucuture to not marry with any exchange
* save candles in sqlite database like gekko, one sqlite data base per exchange    

### To Do listfile

Things to modify and/or add to the program to make it plausible and as good as possible.

DOCUMENTAR DOCUMENTAR DOCUMENTAR
DOCUMENTAR DOCUMENTAR DOCUMENTAR
DOCUMENTAR DOCUMENTAR DOCUMENTAR

BTW, this is a nice practice to do the above.
example:
```python 
def someFunction(param1, param2):
    '''
    Function description
    .
    .
    :param param1: brief description
    :type param1: type
    :param param2:brief description
    :type param2: type
    tags with style
    @warning
    @todo
    @someTag, LiClipse has many default completations (Ctrl + space)
    '''
```

#### Core changes

* only UTS times should be used except when print to human format.

#### Functionality changes

* create and/or add trading strategies.
* implement into the strategies the amounts (besides the price) to sell or buy.
* implement indicators that measure the derivatives of other indicators.

#### STRUCTURE of evaluation

For each coinPair there are several elemental strategies.
For each coinPair there is one cpStrategy englobing all elemental strategies.
for each coinPair there is one elemental evaluator (maybe the same cpStrategy) that decides if an event is ocurring.
For all coinPairs there is one evaluator that compares all cp evaluations and decides trading.
